﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace cga_coursework
{
    public class Game1 : Game
    {
        /// <summary>
        /// Graphics Device Manager instance
        /// </summary>
        private GraphicsDeviceManager _graphics;

        /// <summary>
        /// Sprite batch instance
        /// </summary>
        private SpriteBatch _spriteBatch;

        /// <summary>
        /// Default Construtor
        /// </summary>
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;


        }

        /// <summary>
        /// Initialize Game
        /// </summary>
        protected override void Initialize()
        {
            Window.IsBorderless = true;
            _graphics.IsFullScreen = false;
            // Window.AllowUserResizing = true;
            //_graphics.IsFullScreen = true;
            _graphics.PreferredBackBufferWidth = 1920;
            _graphics.PreferredBackBufferHeight = 1080;


            _graphics.ApplyChanges();

            // this.Window.IsBorderless = true;

            ScreenDimensionManager.Initialize(GraphicsDevice.Viewport.TitleSafeArea.Width, GraphicsDevice.Viewport.TitleSafeArea.Height);

            IntroScreenManager.onClickQuit += QuitNSaveData;
            MainGameManager.Initialize();
            EndScreenManager.onClickQuit += QuitNSaveData;

            // INITAITE FSM
            //FSMGameManager.EnterGameState(EGameState.IntroScreen);

            base.Initialize();


        }

        /// <summary>
        /// Load game resources
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
           
            ScreenDimensionManager.Initialize(GraphicsDevice.Viewport.TitleSafeArea.Width, GraphicsDevice.Viewport.TitleSafeArea.Height);

            IntroScreenManager.LoadData(Content);
            MainGameManager.LoadData(Content, GraphicsDevice);
            EndScreenManager.LoadData(Content);


            // INITAITE FSM
            FSMGameManager.Init();
        }

        /// <summary>
        /// Unloads game resources
        /// </summary>
        protected override void UnloadContent()
        {
            //IntroScreenManager.UnloadContent();
            MainGameManager.UnloadContent();
            EndScreenManager.UnloadContent();

            base.UnloadContent();
        }

        /// <summary>
        /// Save game data and Quit the game
        /// </summary>
        private void QuitNSaveData()
        {
            SavingManager.SaveData();
            Exit();
        }

        /// <summary>
        /// Main Game Loop 
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                //SavingManager.SaveData();
                //Exit();
                QuitNSaveData();
            }

            InputManager.Update(gameTime);
            IntroScreenManager.Update(gameTime);
            MainGameManager.Update(gameTime, GraphicsDevice);
            EndScreenManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Draw (Render) object to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            IntroScreenManager.Draw(_spriteBatch);
            MainGameManager.Draw(_spriteBatch, GraphicsDevice);
            EndScreenManager.Draw(_spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
