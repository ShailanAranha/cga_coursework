﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace cga_coursework
{
    internal abstract class State
    {
        /// <summary>
        /// On Enter state
        /// </summary>
        /// <param name="owner"></param>
        public abstract void Enter(object owner);

        /// <summary>
        /// On Exit state
        /// </summary>
        /// <param name="owner"></param>
        public abstract void Exit(object owner);

        /// <summary>
        /// On Execute
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="gameTime"></param>
        public abstract void Execute(object owner, GameTime gameTime);

        /// <summary>
        /// State name
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Possible trasitions form this state
        /// </summary>
        private List<Transition> m_Transitions = new List<Transition>();
        public List<Transition> Transitions
        {
            get { return m_Transitions; }
        }

        /// <summary>
        /// Add transition to the state
        /// </summary>
        /// <param name="transition"></param>
        public void AddTransition(Transition transition)
        {
            m_Transitions.Add(transition);
        }
    }
}
