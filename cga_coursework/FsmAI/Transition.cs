﻿using System;

namespace cga_coursework
{
    internal class Transition
    {
        /// <summary>
        /// Destition state
        /// </summary>
        public readonly State NextState;

        /// <summary>
        /// Conidtion for transition
        /// </summary>
        public readonly Func<bool> Condition;

        /// <summary>
        /// Default constructior
        /// </summary>
        /// <param name="nextState"></param>
        /// <param name="condition"></param>
        public Transition(State nextState, Func<bool> condition)
        {
            NextState = nextState;
            Condition = condition;
        }

    }
}
