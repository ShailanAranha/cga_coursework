﻿using Microsoft.Xna.Framework;

namespace cga_coursework
{
    class IdleState : State
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public IdleState()
        {
            Name = "Idle";
        }

        /// <summary>
        /// On Enter state
        /// </summary>
        /// <param name="owner"></param>
        public override void Enter(object owner)
        {
            //NpcManager.ActivateCatAnimation(true);
            //NpcManager.ResumeAnimationLoop();

            NpcManager l_npc = owner as NpcManager;

            l_npc.ActivateCatAnimation(true);
            l_npc.ResumeAnimationLoop();
        }

        /// <summary>
        /// On Execute
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="gameTime"></param>
        public override void Execute(object owner, GameTime gameTime)
        { 

        }

        /// <summary>
        /// On Exit state
        /// </summary>
        /// <param name="owner"></param>
        public override void Exit(object owner)
        {

        }
    }
}
