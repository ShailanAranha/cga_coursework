﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace cga_coursework
{
    class FsmAI
    {
        /// <summary>
        /// Owner
        /// </summary>
        private object m_Owner;
        
        /// <summary>
        /// Collection of states
        /// </summary>
        private List<State> m_States;

        /// <summary>
        /// Current active state in FSM
        /// </summary>
        private State m_CurrentState;

        /// <summary>
        /// Defauklt contructor
        /// </summary>
        public FsmAI()
        : this(null)
        {
        }

        /// <summary>
        /// Parameterized constructor to pass instance of the owner
        /// </summary>
        /// <param name="owner"></param>
        public FsmAI(object owner)
        {
            m_Owner = owner;
            m_States = new List<State>();
            m_CurrentState = null;
        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="stateName"></param>
        public void Initialise(string stateName)
        {
            m_CurrentState = m_States.Find(state => state.Name.Equals(stateName));
            if (m_CurrentState != null)
            {
                m_CurrentState.Enter(m_Owner);
            }
        }

        /// <summary>
        /// Add state in the FSM
        /// </summary>
        /// <param name="state"></param>
        public void AddState(State state)
        {
            m_States.Add(state);
        }

        /// <summary>
        /// Game loop
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            // Null check the current state of the FSM
            if (m_CurrentState == null) return;
            // Check the conditions for each transition of the current state
            foreach (Transition t in m_CurrentState.Transitions)
            {
                // If the condition has evaluated to true
                // then transition to the next state
                if (t.Condition())
                {
                    m_CurrentState.Exit(m_Owner);
                    m_CurrentState = t.NextState;
                    m_CurrentState.Enter(m_Owner);
                    break;
                }
            }
            // Execute the current state
            m_CurrentState.Execute(m_Owner, gameTime);
        }
    }
}
