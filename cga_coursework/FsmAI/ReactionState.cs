﻿using Microsoft.Xna.Framework;

namespace cga_coursework
{
    class ReactionState : State
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public ReactionState()
        {
            Name = "Reaction";
        }

        /// <summary>
        /// On Enter state
        /// </summary>
        /// <param name="owner"></param>
        public override void Enter(object owner)
        {
            NpcManager l_npc = owner as NpcManager;
            l_npc.PauseAnimationLoop();

            l_npc.StopMeow();
            l_npc.LoopMeow( true);
            l_npc.PlayMeow();
        }

        /// <summary>
        /// On Execute
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="gameTime"></param>
        public override void Execute(object owner, GameTime gameTime)
        {
            NpcManager l_npc = owner as NpcManager;
            l_npc.CollsionDetection();
        }

        /// <summary>
        /// On Exit state
        /// </summary>
        /// <param name="owner"></param>
        public override void Exit(object owner)
        {
            NpcManager l_npc = owner as NpcManager;
      
            l_npc.ResumeAnimationLoop();
            l_npc.LoopMeow( false);
            //AudioManager.StopSFX(GameAudio.CAT_MEOW);
        }
    }
}
