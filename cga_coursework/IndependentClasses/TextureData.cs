﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class TextureData
    {
        /// <summary>
        /// Texture Id (Readonly as the value can only be set inside the constructor)
        /// </summary>
        public readonly string Id = null;

        /// <summary>
        /// Texture image just loaded in RAM (Readonly as the value can only be set inside the constructor)
        /// </summary>
        public readonly Texture2D TexImg = null;

        /// <summary>
        /// Default contructor, Add data to the object
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_texImag"></param>
        public TextureData(string a_strId, Texture2D a_texImag)
        {
            Id = a_strId;
            TexImg = a_texImag;
        }
    }
}
