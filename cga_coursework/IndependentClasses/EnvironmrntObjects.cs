﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;


namespace cga_coursework
{

    /// <summary>
    /// Interactive object of the environment (Player can interact) 
    /// </summary>
    class InteractableObjects
    {

        /// <summary>
        /// Collection of interactive objects
        /// </summary>
        private List<GameObject> m_lstObjects = new List<GameObject>();
        public IReadOnlyList<GameObject> Objects { get { return m_lstObjects; } }

        /// <summary>
        /// Players position on the previous iteration
        /// </summary>
        private Vector2 playerOldPosition = Vector2.Zero;

        /// <summary>
        /// Event invoked with player colldier with an interactive objects
        /// </summary>
        public event Action<string> OnCollisionWithInteractiveObjects = null;

        /// <summary>
        /// Clock
        /// </summary>
        private double m_timer = 0;

        /// <summary>
        /// Initiate timer
        /// </summary>
        private bool m_isInitiateTimer = false;
        
        /// <summary>
        /// Store last collider object id
        /// </summary>
        private string m_strLastCollidedObjectId = null;

        /// <summary>
        /// Interactive object instances
        /// </summary>
        #region OBJECT INISTANCES

        private GameObject m_objDoor;
        private GameObject m_objPainting1;
        private GameObject m_objPainting2;
        private GameObject m_objPainting3;
        private GameObject m_objSafe;
        private GameObject m_objTable;

        #endregion

        /// <summary>
        /// Ids and asset name in content should match
        /// </summary>
        #region OBJECT CONSTANT IDS

        public const string Door_ID = "Door";
        public const string Painting1_ID = "painting_render_1";
        public const string Painting2_ID = "painting_render_2";
        public const string Painting3_ID = "painting_render_3";

        public const string Safe_ID = "safe";
        public const string Table_ID = "Table";

        #endregion

        /// <summary>
        /// Defaut contructor
        /// </summary>
        public InteractableObjects()
        {
            // basic values
            float l_fltScreenWidth = ScreenDimensionManager.ScreenDimensions.X;
            float l_fltScreenHeight = ScreenDimensionManager.ScreenDimensions.Y;
            float l_fltWallOffset = PlayerManager.WALL_OFFSET;

            // Initialize objects
            m_objDoor = new GameObject(Door_ID, new Vector2(PlayerManager.WALL_OFFSET, 700), new Vector2(0.25f, 0.25f), new Vector2(PlayerManager.WALL_OFFSET, 700), new Vector2(0.35f, 0.25f), EGuidanceMessage.Message2);
            m_objPainting1 = new GameObject(Painting1_ID, new Vector2(l_fltScreenWidth / 2 + 300.0f, l_fltWallOffset), new Vector2(0.25f, 0.25f), new Vector2(l_fltScreenWidth / 2 + 300.0f, l_fltWallOffset), new Vector2(0.25f, 0.25f), EGuidanceMessage.Message2);
            m_objPainting2 = new GameObject(Painting2_ID, new Vector2(l_fltScreenWidth / 2 - 300.0f, l_fltWallOffset), new Vector2(0.25f, 0.25f), new Vector2(l_fltScreenWidth / 2 - 300.0f, l_fltWallOffset), new Vector2(0.25f, 0.25f), EGuidanceMessage.Message2);
            m_objPainting3 = new GameObject(Painting3_ID, new Vector2(l_fltScreenWidth / 2 + 20, l_fltScreenHeight - l_fltWallOffset - 50), new Vector2(0.25f, 0.25f), new Vector2(l_fltScreenWidth / 2 + 20, l_fltScreenHeight - l_fltWallOffset - 50), new Vector2(0.25f, 0.25f), EGuidanceMessage.Message2);

            m_objSafe = new GameObject(Safe_ID, new Vector2(l_fltScreenWidth / 2, l_fltWallOffset), new Vector2(0.25f, 0.25f), new Vector2(l_fltScreenWidth / 2, l_fltWallOffset), new Vector2(0.25f, 0.25f), EGuidanceMessage.Message2);
            SafeManager.OnSuccessfullPinEntry += () => { m_objSafe.GuidanceMessage = EGuidanceMessage.Message3; };
            SavingManager.OnDataFlushed += () => { m_objSafe.GuidanceMessage = EGuidanceMessage.Message2; };

            m_objTable = new GameObject(Table_ID, new Vector2(l_fltScreenWidth / 2 - 100.0f, l_fltScreenHeight / 2 - 100.0f), new Vector2(0.25f, 0.25f), new Vector2(l_fltScreenWidth / 2 - 100.0f, l_fltScreenHeight / 2 - 100.0f), new Vector2(0.25f, 0.25f), EGuidanceMessage.Message2);

            // add object to the list
            m_lstObjects.Add(m_objDoor);
            m_lstObjects.Add(m_objPainting1);
            m_lstObjects.Add(m_objPainting2);
            m_lstObjects.Add(m_objPainting3);
            m_lstObjects.Add(m_objSafe);
            m_lstObjects.Add(m_objTable);
        }

        /// <summary>
        /// Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        /// <param name="a_player"></param>
        public void Update(GameTime a_gameTime, Player a_player)
        {
            Rectangle BoundingRectangle = new Rectangle((int)(a_player.CurrentPosition.X - 20.0f), (int)(a_player.CurrentPosition.Y - 25.0f), a_player.Width * 2, a_player.Height * 2);

            // Rectangle BoundingRectangle = new Rectangle((int)(a_player.CurrentPosition.X - 50.0f), (int)(a_player.CurrentPosition.Y - 50.0f), a_player.Width / 2 - 10, a_player.Height);
            // Rectangle BoundingRectangle = new Rectangle((int)a_player.CurrentPosition.X, (int)a_player.CurrentPosition.Y, a_player.Width, a_player.Height);
            //Rectangle BoundingRectangle = PlayerManager.Player.BoundingRectangle;

            for (int i = 0; i < m_lstObjects.Count; i++)
            {
                if (m_lstObjects[i].IsCollidable && m_lstObjects[i].BoundingRectangle.Intersects(BoundingRectangle))
                {
                    // use events to implement this, add events to each object
                    a_player.CurrentPosition = playerOldPosition;

                    m_strLastCollidedObjectId = m_lstObjects[i].Id;
                    InputManager.OnPressedOpenBtn -= TriggerCollisionEvent;
                    InputManager.OnPressedOpenBtn += TriggerCollisionEvent;

                    GuidanceManager.ShowGuidanceMessage(m_lstObjects[i].GuidanceMessage);


                    m_isInitiateTimer = true;

                }
            }

            playerOldPosition = a_player.CurrentPosition;

            if (m_isInitiateTimer)
            {
                m_timer += a_gameTime.ElapsedGameTime.TotalMilliseconds;

                if (m_timer > 2000)
                {
                    InputManager.OnPressedOpenBtn -= TriggerCollisionEvent;
                    GuidanceManager.HideGuidanceMessage();
                    m_timer = 0;
                    m_isInitiateTimer = false;
                }
            }
        }

        /// <summary>
        /// Trigger collision 
        /// </summary>
        private void TriggerCollisionEvent()
        {
            InputManager.OnPressedOpenBtn -= TriggerCollisionEvent;
            m_isInitiateTimer = false;
            m_timer = 0;
            OnCollisionWithInteractiveObjects?.Invoke(m_strLastCollidedObjectId);
        }

        /// <summary>
        /// For testing (is collider visible?)
        /// </summary>
        public void ToggleColliders()
        {
            m_objDoor.IsCollidableVisible = !m_objDoor.IsCollidableVisible;
            m_objPainting1.IsCollidableVisible = !m_objPainting1.IsCollidableVisible;
            m_objPainting2.IsCollidableVisible = !m_objPainting2.IsCollidableVisible;
            m_objPainting3.IsCollidableVisible = !m_objPainting3.IsCollidableVisible;
            m_objSafe.IsCollidableVisible = !m_objSafe.IsCollidableVisible;
            m_objTable.IsCollidableVisible = !m_objTable.IsCollidableVisible;
        }

    }

    /// <summary>
    /// Non-Interactive object of environment 
    /// </summary>
    class NonInteractbaleObjects
    {

        /// <summary>
        /// Collection of non-interactive objects
        /// </summary>
        private List<GameObject> m_lstObjects = new List<GameObject>();
        public IReadOnlyList<GameObject> Objects { get { return m_lstObjects; } }

        /// <summary>
        /// Players position on the previous iteration
        /// </summary>
        private Vector2 playerOldPosition = Vector2.Zero;

        /// <summary>
        /// Non-Interactive object instances
        /// </summary>
        #region OBJECT INISTANCES

        private GameObject m_objRoom;
        private GameObject m_objPlant1;
        private GameObject m_objPlant2;
        private GameObject m_objPlant3;
        private GameObject m_objPlant4;
        private GameObject m_objGreenSofa;
        private GameObject m_objBackSofa;
        private GameObject m_objRedChair;
        private GameObject m_objLibrary;

        #endregion


        /// <summary>
        /// Ids and asset name in content should match
        /// </summary>
        #region OBJECT CONSTANT IDS

        private const string ROOM_ID = "Room_outline";
        private const string PLANT_ID = "Plant";
        private const string PLANT2_ID = "Plant2";
        private const string GREEN_SOFA_ID = "green_sofa";
        private const string BLACK_SOFA_ID = "black_sofa";
        private const string RED_CHAIR_ID = "red_chair";
        private const string LIBRARY_ID = "Library";

        #endregion

        /// <summary>
        /// Default contructor
        /// </summary>
        public NonInteractbaleObjects()
        {
            float l_fltScreenWidth = ScreenDimensionManager.ScreenDimensions.X;
            float l_fltScreenHeight = ScreenDimensionManager.ScreenDimensions.Y;
            float l_fltWallOffset = PlayerManager.WALL_OFFSET;

            m_objRoom = new GameObject(ROOM_ID, Vector2.Zero, false);
            m_objPlant1 = new GameObject(PLANT_ID, new Vector2(l_fltWallOffset + 20, 600), new Vector2(0.10f, 0.10f), new Vector2(l_fltWallOffset + 20, 600), new Vector2(0.1f, 0.1f));
            m_objPlant2 = new GameObject(PLANT_ID, new Vector2(l_fltWallOffset + 20, 900), new Vector2(0.10f, 0.10f), new Vector2(l_fltWallOffset + 20, 900), new Vector2(0.1f, 0.1f));
            m_objPlant3 = new GameObject(PLANT_ID, new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltScreenHeight / 1.5f), new Vector2(0.15f, 0.15f), new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltScreenHeight / 1.5f), new Vector2(0.15f, 0.15f));
            m_objPlant4 = new GameObject(PLANT2_ID, new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltWallOffset + 150), new Vector2(0.15f, 0.15f), new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltWallOffset + 150), new Vector2(0.15f, 0.15f));

            m_objGreenSofa = new GameObject(GREEN_SOFA_ID, new Vector2(l_fltScreenWidth / 2 - 300.0f, l_fltScreenHeight - l_fltWallOffset - 100), new Vector2(0.3f, 0.3f), new Vector2(l_fltScreenWidth / 2 - 300.0f, l_fltScreenHeight - l_fltWallOffset - 100), new Vector2(0.3f, 0.3f));
            m_objBackSofa = new GameObject(BLACK_SOFA_ID, new Vector2(l_fltScreenWidth / 2 + 300.0f, l_fltScreenHeight - l_fltWallOffset - 100), new Vector2(0.3f, 0.3f), new Vector2(l_fltScreenWidth / 2 + 300.0f, l_fltScreenHeight - l_fltWallOffset - 100), new Vector2(0.3f, 0.3f));
            m_objRedChair = new GameObject(RED_CHAIR_ID, new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltScreenHeight / 3), new Vector2(0.5f, 0.5f), new Vector2(l_fltScreenWidth - l_fltWallOffset - 100.0f, l_fltScreenHeight / 3), new Vector2(0.5f, 0.5f));

            m_objLibrary = new GameObject(LIBRARY_ID, new Vector2(l_fltWallOffset, l_fltWallOffset), new Vector2(0.75f, 0.75f), new Vector2(l_fltWallOffset, l_fltWallOffset), new Vector2(0.75f, 0.75f));

            m_lstObjects.Add(m_objRoom);
            m_lstObjects.Add(m_objPlant1);
            m_lstObjects.Add(m_objPlant2);
            m_lstObjects.Add(m_objPlant3);
            m_lstObjects.Add(m_objPlant4);
            m_lstObjects.Add(m_objGreenSofa);
            m_lstObjects.Add(m_objBackSofa);
            m_lstObjects.Add(m_objRedChair);
            m_lstObjects.Add(m_objLibrary);
        }

        /// <summary>
        /// Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        /// <param name="a_player"></param>
        public void Update(GameTime a_gameTime, Player a_player)
        {
            Rectangle BoundingRectangle = new Rectangle((int)(a_player.CurrentPosition.X - 20.0f), (int)(a_player.CurrentPosition.Y - 25.0f), a_player.Width * 2, a_player.Height * 2);
            // Rectangle BoundingRectangle = new Rectangle((int)(a_player.CurrentPosition.X - 50.0f), (int)(a_player.CurrentPosition.Y - 50.0f), a_player.Width / 2 - 10, a_player.Height);
            // Rectangle BoundingRectangle = new Rectangle((int)a_player.CurrentPosition.X, (int)a_player.CurrentPosition.Y, a_player.Width, a_player.Height);
            //Rectangle BoundingRectangle = PlayerManager.Player.BoundingRectangle;

            for (int i = 0; i < m_lstObjects.Count; i++)
            {

                if (m_lstObjects[i].IsCollidable && m_lstObjects[i].BoundingRectangle.Intersects(BoundingRectangle))
                {
                    // use events to implement this, add events to each object
                    a_player.CurrentPosition = playerOldPosition;
                }

            }

            playerOldPosition = a_player.CurrentPosition;

        }

        /// <summary>
        /// For testing (is collider visible?)
        /// </summary>
        public void ToggleColliders()
        {
            m_objGreenSofa.IsCollidableVisible = !m_objGreenSofa.IsCollidableVisible;
            m_objBackSofa.IsCollidableVisible = !m_objBackSofa.IsCollidableVisible;
            m_objRedChair.IsCollidableVisible = !m_objRedChair.IsCollidableVisible;
            m_objPlant1.IsCollidableVisible = !m_objPlant1.IsCollidableVisible;
            m_objPlant2.IsCollidableVisible = !m_objPlant2.IsCollidableVisible;
            m_objPlant3.IsCollidableVisible = !m_objPlant3.IsCollidableVisible;
            m_objPlant4.IsCollidableVisible = !m_objPlant4.IsCollidableVisible;
            m_objLibrary.IsCollidableVisible = !m_objLibrary.IsCollidableVisible;
        }
    }
}
