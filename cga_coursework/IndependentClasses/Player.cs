﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace cga_coursework
{
    class Player
    {
        /// <summary>
        /// Animation representing the player
        /// </summary>
        public Texture2D PlayerTexture { get; set; } = null;

        /// <summary>
        /// Player animation object PlayerRightWalkAnim
        /// </summary>
        public Animation PlayerRightWalkAnim;
        public Animation PlayerLeftWalkAnim;
        public Animation PlayerUpWalkAnim;
        public Animation PlayerDownWalkAnim;

        /// <summary>
        /// Current PLayer movement type
        /// </summary>
        public EPlayerMOvement m_currentMovement = EPlayerMOvement.Down;

        /// <summary>
        /// CurrentPosition of the player relative to the upper left side of the screen
        /// </summary>
        public Vector2 CurrentPosition = Vector2.Zero ;
        //private Vector2 m_cuuPos = Vector2.Zero;
        //public Vector2 CurrentPosition
        //{
        //    get 
        //    {
        //        return m_cuuPos;
        //    }

        //    set
        //    {
        //        m_cuuPos = value;
        //        SavingManager.SavePlayerPosition(m_cuuPos);
        //    }
        //}

        /// <summary>
        /// Player Start positin
        /// </summary>
        public Vector2 StartPosition = Vector2.Zero;

        /// <summary>
        /// State of the player
        /// </summary>
        public bool Active { get; set; } = false;

        /// <summary>
        /// Get the width of the player ship
        /// </summary>
        public int Width
        {
            get
            {
                //return ;
                return PlayerRightWalkAnim.m_iFrameWidth;
            }
        }

        /// <summary>
        /// Get the height of the player ship
        /// </summary>
        public int Height
        {
            get
            {
                return PlayerRightWalkAnim.m_iFrameHeight;
            }
        }

        /// <summary>
        /// Bounding area
        /// </summary>
        public Rectangle BoundingRectangle;

        /// <summary>
        /// Is Collider visible on the screen?
        /// </summary>
        public bool IsCollidableVisible { get; set; } = false;

        /// <summary>
        /// Initilaize player properties
        /// </summary>
        /// <param name="a_textIdle"></param>
        /// <param name="a_animRight"></param>
        /// <param name="a_animLeft"></param>
        /// <param name="a_animUp"></param>
        /// <param name="a_animDown"></param>
        /// <param name="a_vec2Position"></param>
        public void Initialize(Texture2D a_textIdle, Animation a_animRight, Animation a_animLeft, Animation a_animUp, Animation a_animDown, Vector2 a_vec2Position)
        {
            PlayerTexture = a_textIdle;

            PlayerRightWalkAnim = a_animRight;
            PlayerLeftWalkAnim = a_animLeft;
            PlayerUpWalkAnim = a_animUp;
            PlayerDownWalkAnim = a_animDown;

            // CurrentPosition = a_vec2Position;
            StartPosition = a_vec2Position;
            Active = true;

            CurrentPosition = SavingManager.SavedData.PlayerCurrentPosition;
        }

        /// <summary>
        /// Set players start position
        /// </summary>
        public void SetPlayerToStartPosition()
        {
            CurrentPosition = StartPosition;
        }

        /// <summary>
        /// Game loop to handle player events
        /// </summary>
        /// <param name="a_gameTime"></param>
        public void Update(GameTime a_gameTime)
        {
           // BoundingRectangle = new Rectangle((int)(CurrentPosition.X - 50.0f), (int)(CurrentPosition.Y - 50.0f), Width/2 -10, Height);
            BoundingRectangle = new Rectangle((int)(CurrentPosition.X - 20.0f), (int)(CurrentPosition.Y - 25.0f), Width*2, Height * 2);

            switch (m_currentMovement)
            {
                case EPlayerMOvement.Right:
                    PlayerRightWalkAnim.m_vec2Position = CurrentPosition;
                    PlayerRightWalkAnim.Update(a_gameTime);

                    break;
                case EPlayerMOvement.Left:
                    PlayerLeftWalkAnim.m_vec2Position = CurrentPosition;
                    PlayerLeftWalkAnim.Update(a_gameTime);

                    break;
                case EPlayerMOvement.Up:
                    PlayerUpWalkAnim.m_vec2Position = CurrentPosition;
                    PlayerUpWalkAnim.Update(a_gameTime);

                    break;
                case EPlayerMOvement.Down:
                    PlayerDownWalkAnim.m_vec2Position = CurrentPosition;
                    PlayerDownWalkAnim.Update(a_gameTime);

                    break;

                default:
                    break;
            }

        }

        /// <summary>
        /// Draw player on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public void Draw(SpriteBatch a_spriteBatch)
        {
            switch (m_currentMovement)
            {
                case EPlayerMOvement.Right:
                    PlayerRightWalkAnim.Draw(a_spriteBatch);

                    break;
                case EPlayerMOvement.Left:
                    PlayerLeftWalkAnim.Draw(a_spriteBatch);

                    break;
                case EPlayerMOvement.Up:
                    PlayerUpWalkAnim.Draw(a_spriteBatch);

                    break;
                case EPlayerMOvement.Down:
                    PlayerDownWalkAnim.Draw(a_spriteBatch);

                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Draw player collider on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        /// <param name="a_graphicDevice"></param>
        public void DrawCollider(SpriteBatch a_spriteBatch, GraphicsDevice a_graphicDevice)
        {
            if (IsCollidableVisible)
            {

                Rectangle l_bound = PlayerManager.Player.BoundingRectangle;
                Texture2D l_rectPlayer = new Texture2D(a_graphicDevice, l_bound.Width, l_bound.Height);

                Color[] l_colPlayer = new Color[l_bound.Width * l_bound.Height];
                for (int i = 0; i < l_colPlayer.Length; i++) l_colPlayer[i] = Color.Chocolate;
                l_rectPlayer.SetData(l_colPlayer);

                a_spriteBatch.Draw(l_rectPlayer, l_bound, Color.White);
            }
        }
    }

}
