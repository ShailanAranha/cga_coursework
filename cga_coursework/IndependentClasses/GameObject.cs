﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace cga_coursework
{
    public class GameObject
    {

        /// <summary>
        /// GameObject ID
        /// </summary>
        public string Id { get; protected set; }
        
        /// <summary>
        /// Texture of the object
        /// </summary>
        public Texture2D Texture { get; protected set; } = null;
        
        /// <summary>
        /// Position of the gameobject
        /// </summary>
        public Vector2 Position { get; protected set; } = Vector2.Zero;

        /// <summary>
        /// Position of the collider
        /// </summary>
        public Vector2 ColliderPosition { get; private set; } = Vector2.Zero;

        /// <summary>
        /// Display size of the object
        /// </summary>
        public Vector2 DisplayScale { get; protected set; } = Vector2.Zero;

        /// <summary>
        /// Collider size of the object
        /// </summary>
        public Vector2 ColliderScale { get; protected set; } = Vector2.Zero;

        /// <summary>
        /// Is Object collidable with the player?
        /// </summary>
        public bool IsCollidable { get; set; } = true;

        /// <summary>
        /// Bounding Rect of the object
        /// </summary>
        public Rectangle BoundingRectangle { get; private set; }

        /// <summary>
        /// Guidance message if collided with this object
        /// </summary>
        public EGuidanceMessage GuidanceMessage { get;  set; } = EGuidanceMessage.None;

        /// <summary>
        /// Is Collider visible on the screen?(For testing)
        /// </summary>
        public bool IsCollidableVisible { get; set; } = false;


        //public GameObject(string a_strId, Vector2 a_vec2Position)
        //{
        //    Id = a_strId;
        //    Position = a_vec2Position;
        //    IsCollidable = true;
        //}

        /// <summary>
        /// Constructor to intantiate object
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_vec2DisplayScale"></param>
        /// <param name="a_vecColliderPosition"></param>
        /// <param name="a_vec2ColliderScale"></param>
        /// <param name="a_isCollidable"></param>
        public GameObject(string a_strId, Vector2 a_vec2Position, Vector2 a_vec2DisplayScale, Vector2 a_vecColliderPosition, Vector2 a_vec2ColliderScale, bool a_isCollidable = true)
        {
            Id = a_strId;
            Position = a_vec2Position;
            ColliderPosition = a_vecColliderPosition;
            IsCollidable = a_isCollidable;
            DisplayScale = a_vec2DisplayScale;
            ColliderScale = a_vec2ColliderScale;

        }

        /// <summary>
        /// Constructor to intantiate object with a guidance message
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_vec2DisplayScale"></param>
        /// <param name="a_vecColliderPosition"></param>
        /// <param name="a_vec2ColliderScale"></param>
        /// <param name="a_eGuidanceMessage"></param>
        /// <param name="a_isCollidable"></param>
        public GameObject(string a_strId, Vector2 a_vec2Position, Vector2 a_vec2DisplayScale, Vector2 a_vecColliderPosition, Vector2 a_vec2ColliderScale, 
            EGuidanceMessage a_eGuidanceMessage, bool a_isCollidable = true)
        {
            Id = a_strId;
            Position = a_vec2Position;
            ColliderPosition = a_vecColliderPosition;
            IsCollidable = a_isCollidable;
            DisplayScale = a_vec2DisplayScale;
            ColliderScale = a_vec2ColliderScale;
            GuidanceMessage = a_eGuidanceMessage;
        }

        /// <summary>
        /// Constructor to initantiate objects with unity scale of diaply and colldiers
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_isCollidable"></param>
        public GameObject(string a_strId, Vector2 a_vec2Position, bool a_isCollidable = true)
        {
            Id = a_strId;
            Position = a_vec2Position;
            IsCollidable = a_isCollidable;
            DisplayScale = new Vector2(1, 1);
            ColliderScale = new Vector2(1, 1);
        }

        /// <summary>
        /// Contructor to intantiate object that doesn't require collision
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_vec2DisplayScale"></param>
        public GameObject(string a_strId, Vector2 a_vec2Position, Vector2 a_vec2DisplayScale)
        {
            Id = a_strId;
            Position = a_vec2Position;
            DisplayScale = a_vec2DisplayScale;
            IsCollidable = false;
            ColliderScale = new Vector2(1, 1);
        }

        /// <summary>
        /// Initilaize gameobject properties
        /// </summary>
        /// <param name="a_texture"></param>
        public virtual void Initialize(Texture2D a_texture)
        {
            Texture = a_texture;
            BoundingRectangle = new Rectangle((int)ColliderPosition.X, (int)ColliderPosition.Y, (int)(Texture.Width * ColliderScale.X), (int)(Texture.Height * ColliderScale.Y));
        }

        /// <summary>
        /// Game loop to update gameobject elements
        /// </summary>
        /// <param name="a_gameTime"></param>
        public virtual void Update(GameTime a_gameTime)
        {

        }

        /// <summary>
        /// Draw object on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public virtual void Draw(SpriteBatch a_spriteBatch)
        {
            a_spriteBatch.Draw(Texture, Position, null, Color.White, 0.0f, Vector2.Zero, DisplayScale, SpriteEffects.None, 0.0f);
        }

        /// <summary>
        /// Draw collider on the screen (For Texting)
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        /// <param name="a_graphicDevice"></param>
        public virtual void DrawCollider(SpriteBatch a_spriteBatch, GraphicsDevice a_graphicDevice)
        { 
        
            if (IsCollidableVisible)
            {
                Texture2D rect = new Texture2D(a_graphicDevice, BoundingRectangle.Width, BoundingRectangle.Height);

                Color[] data = new Color[BoundingRectangle.Width * BoundingRectangle.Height];
                for (int i = 0; i < data.Length; i++) data[i] = Color.Black;
                rect.SetData(data);
                a_spriteBatch.Draw(rect, BoundingRectangle,Color.White);

            }
        }
    }
}
