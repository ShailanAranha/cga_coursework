﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace cga_coursework
{
    class Button : GameObject
    {
        /// <summary>
        /// Normal state texture of the button
        /// </summary>
        public readonly Texture2D texNormalState = null;

        /// <summary>
        /// Highlighed state texture of the button
        /// </summary>
        public readonly Texture2D texHightlightedState = null;

        /// <summary>
        /// On click button event
        /// </summary>
        public event Action OnClick = null;

        /// <summary>
        /// Is event invoked?
        /// </summary>
        private bool IsEventFired { get; set; } = false;

        /// <summary>
        /// Is button selected?
        /// </summary>
        public bool IsSelected { get; private set; } = false;

        /// <summary>
        /// Is OnClick event ready to be invoked, sets true if the previous input is released (INORDER TO AVOID THE PREVIOUS INPUT TRIGGER)
        /// </summary>
        private bool IsEventActivated { get; set; } = false;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_vec2DisplayScale"></param>
        /// <param name="a_texNormalState"></param>
        /// <param name="a_texHighlighted"></param>
        public Button(string a_strId, Vector2 a_vec2Position, Vector2 a_vec2DisplayScale, Texture2D a_texNormalState, Texture2D a_texHighlighted) : base(a_strId, a_vec2Position, a_vec2DisplayScale)
        {
            Id = a_strId;
            Position = a_vec2Position;
            DisplayScale = a_vec2DisplayScale;
            IsCollidable = false;
            ColliderScale = new Vector2(1, 1);
            texNormalState = a_texNormalState;
            texHightlightedState = a_texHighlighted;

            this.Texture = texNormalState; // default
        }

        /// <summary>
        /// Change button state to highlighted
        /// </summary>
        public void SetHighlightedState()
        {
            this.Texture = texHightlightedState;
            IsSelected = true;
        }

        /// <summary>
        /// Change button state to be normal
        /// </summary>
        public void SetNormalState()
        { 
            this.Texture = texNormalState;
            IsSelected = false;
        }

        /// <summary>
        /// Game Loop for button
        /// </summary>
        /// <param name="a_gameTime"></param>
        public override void Update(GameTime a_gameTime)
        {
            base.Update(a_gameTime);

            if (!IsSelected)
            {
                return;
            }

            if (InputManager.CurrentKeyboardState.IsKeyDown(Keys.Enter) || InputManager.CurrentGamePadState.Buttons.A == ButtonState.Pressed)
            {
                if (IsEventActivated && !IsEventFired)
                {
                    OnClick?.Invoke();
                    IsEventFired = true;
                }

            }

            //if (InputManager.CurrentKeyboardState.IsKeyUp(Keys.Enter) || InputManager.CurrentGamePadState.Buttons.A == ButtonState.Released)
            if (InputManager.CurrentKeyboardState.IsKeyUp(Keys.Enter) && InputManager.CurrentGamePadState.Buttons.A == ButtonState.Released)
            {
                IsEventFired = false;
                IsEventActivated = true; // INORDER TO AVOID THE PREVIOUS INPUT TRIGGER
            }
        }

        /// <summary>
        /// Previous input boolean check value reset
        /// </summary>
        public void ResetEvent()
        {
           IsEventActivated = false;
        }
    }
}
