﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    /// <summary>
    /// All the audios and sfx ids are store in the class
    /// </summary>
    class GameAudio
    {
        //AUDIO
        public const string INTRO_GAME = "Intro";
        public const string MAIN_GAME = "main_game2";
        public const string END_GAME = "Intro";

        // SFX
        public const string BUTTON_TRANSITION = "button_transition";
        public const string BUTTON_CLICK = "buton_click";
        public const string CORRECT_PIN = "safe_open";
        public const string INCORRECT_PIN = "IncorrectPin";
        public const string TYPING_PIN = "pin_typing";
        public const string OPEN_CLUE = "clue_open";
        public const string PAGE_TURN = "page_turn";
        public const string CAT_MEOW = "cat_meow";


    }
}
