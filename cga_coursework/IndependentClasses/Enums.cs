﻿namespace cga_coursework
{

    // *********** FSM STATES ****************

    /// <summary>
    /// Type of game states
    /// </summary>
    public enum EGameState : byte
    {
        IntroScreen = 0, // menu
        MainGame = 1, //  main game
        EndScreen = 2 // restart screen with score
    }

    // *********** GAME DATA ****************

    /// <summary>
    /// Types of narrative
    /// </summary>
    public enum ENarrativeType : byte
    {
        None = 0,
        Intro = 1,
        PostSafeOpen = 2,
        End = 3
    }

    // *********** PLAYER ****************

    /// <summary>
    /// Type of player movement
    /// </summary>
    public enum EPlayerMOvement
    {
        Right, Left, Up, Down
    }

    // *********** INTRO SCREEN ****************

    /// <summary>
    /// Play screen button options
    /// </summary>
    public enum EStartScreenOptions : byte
    {
        Continue = 0,
        Play = 1,
        Controls = 2,
        Exit = 3
    }

    /// <summary>
    /// Types of screen inthe intro game
    /// </summary>
    public enum EStartScreen : byte
    {
        PlayScreen = 0,
        ControlsScreen = 1,
    }

    // *********** END SCREEN ****************

    /// <summary>
    /// End screen button options
    /// </summary>
    public enum EEndScreenOptions : byte
    {
        Restart = 0,
        Quit = 1,
    }

    /// <summary>
    /// Total end screen result conditions
    /// </summary>
    public enum EEndGameResult : byte
    {
        NO_PIN_NO_KEY,
        NO_PIN_YES_KEY,
        YES_PIN_NO_KEY,
        YES_PIN_YES_KEY
    }

    // *********** GUIDANCE TYPES ****************

    /// <summary>
    /// Types of Guidance messages
    /// </summary>
    public enum EGuidanceMessage : byte
    {
        None = 0,
        Message1 = 1,
        Message2 = 2,
        Message3 = 3,
        Message4 = 4,
        Message5 = 5,
    }
}
