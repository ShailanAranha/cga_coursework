﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace cga_coursework
{
    /// <summary>
    /// Store all game data (Thats stored outside the build)
    /// </summary>
    [Serializable()]
    [XmlRoot("GameData")]
    public class GameData
    {
        /// <summary>
        /// All narrative data
        /// </summary>
        [XmlElement("Narrative")]
        public Narrative Narrative { get; set; }

        /// <summary>
        ///  Data for clues related to pin
        /// </summary>
        [XmlElement("PinClue")]
        public PinClue PinClue { get; set; }

        /// <summary>
        /// Data for clues related to key
        /// </summary>
        [XmlElement("KeyClue")]
        public KeyClue KeyClue { get; set; }

        /// <summary>
        /// Three-Digit pin to open the safe
        /// </summary>
        [XmlElement("Pin")]
        public string Pin { get; set; }

        /// <summary>
        /// Two-Digit to open the door
        /// </summary>
        [XmlElement("Key")]
        public string Key { get; set; }

        /// <summary>
        /// Header text on top of game timer
        /// </summary>
        [XmlElement("EnemyHeader")]
        public string EnemyHeader { get; set; }
        
        /// <summary>
        /// Game timer (reverse clock)
        /// </summary>
        [XmlElement("GameTime")]
        public string GameTime { get; set; }

        /// <summary>
        /// End result data if player fails to figure out both key and pin
        /// </summary>
        [XmlElement("NO_PIN_NO_KEY")]
        public EndResult NO_PIN_NO_KEY { get; set; }

        /// <summary>
        /// End result data if player fails to figure out pin but manages to find Key
        /// </summary>
        [XmlElement("NO_PIN_YES_KEY")]
        public EndResult NO_PIN_YES_KEY { get; set; }

        /// <summary>
        /// End result data if player fails to figure out key but manages to find Pin
        /// </summary>
        [XmlElement("YES_PIN_NO_KEY")]
        public EndResult YES_PIN_NO_KEY { get; set; }

        /// <summary>
        /// End result data if player successfully finds out both key and pin
        /// </summary>
        [XmlElement("YES_PIN_YES_KEY")]
        public EndResult YES_PIN_YES_KEY { get; set; }
    }

    /// <summary>
    /// Clue 
    /// </summary>
    [Serializable()]
    public class Clue
    {
        /// <summary>
        /// Clue Id
        /// </summary>
        public string ClueId { get; set; }

        /// <summary>
        /// Texture Id in the xml should same as the file name of the texture image with extension (.png, .jpg)
        /// </summary>
        public string TextureId { get; set; }

        /// <summary>
        /// Question text for the clue
        /// </summary>
        public string DisplayText { get; set; }

        /// <summary>
        /// Is Clue on display?
        /// </summary>
        [NonSerialized]
        public bool IsDisplayClue  = false;

        /// <summary>
        /// POsition of the querstion text
        /// </summary>
        [XmlElement("TextPostion")]
        public string TextPostion { get; set; }

        /// <summary>
        /// Position of the cluse image
        /// </summary>
        [XmlElement("ImagePostion")]
        public string ImagePostion { get; set; }

        /// <summary>
        ///  Size of clue image
        /// </summary>
        [XmlElement("ImageDimensions")]
        public string ImageDimensions { get; set; }
    }

    /// <summary>
    /// Store data of all clues to open the safe (Pin Clues)
    /// </summary>
    [Serializable()]
    public class PinClue
    {
        /// <summary>
        /// First clues for Pin
        /// </summary>
        [XmlElement("FirstClue")]
        public Clue FirstClue { get; set; }

        /// <summary>
        /// Second clues for Pin
        /// </summary>
        [XmlElement("SecondClue")]
        public Clue SecondClue { get; set; }

        /// <summary>
        /// Third clues for Pin
        /// </summary>
        [XmlElement("ThirdClue")]
        public Clue ThirdClue { get; set; }
    }

    /// <summary>
    /// Store data of all clues to open the door (key Clues)
    /// </summary>
    [Serializable()]
    public class KeyClue
    {
        /// <summary>
        /// Clues for Key
        /// </summary>
        [XmlElement("Clue")]
        public Clue Clue { get; set; }
    }

    /// <summary>
    /// Narrative parren (Hanldes all naratives)
    /// </summary>
    [Serializable()]
    public class Narrative
    {
        /// <summary>
        /// Narrative script when game begins
        /// </summary>
        [XmlElement("Intro")]
        public List<NarrativePage> Intro { get; set; }

        /// <summary>
        /// Narrative script when player opens the safe
        /// </summary>
        [XmlElement("PostSafeOpen")]
        public List<NarrativePage> PostSafeOpen { get; set; }

        /// <summary>
        /// Narrative script when player opens the door
        /// </summary>
        [XmlElement("End")]
        public List<NarrativePage> End { get; set; }

        /// <summary>
        /// Narrative background file name
        /// </summary>
        [XmlElement("NarrativeBg")]
        public string NarrativeBg { get; set; }

        //[NonSerialized]
        // public GameObject Object = null;
    }

    /// <summary>
    /// Narrative Page
    /// </summary>
    [Serializable]
    public class NarrativePage
    {
        /// <summary>
        /// Narrative ID
        /// </summary>
        [XmlElement("Id")]
        public string Id { get; set; }

        /// <summary>
        /// Narratve text
        /// </summary>
        [XmlElement("DisplayText")]
        public string DisplayText { get; set; }

        /// <summary>
        /// Position of narartive text
        /// </summary>
        [XmlElement("TextPostion")]
        public string TextPostion { get; set; }

        /// <summary>
        /// Position of background 
        /// </summary>
        [XmlElement("BgPostion")]
        public string BgPostion { get; set; }

        /// <summary>
        /// Size of background
        /// </summary>
        [XmlElement("BgDimensions")]
        public string BgDimensions { get; set; }

        /// <summary>
        /// Type of narrative 
        /// </summary>
        [NonSerialized]
        public ENarrativeType Type = ENarrativeType.None;

        /// <summary>
        /// Is narrative currently on display?
        /// </summary>
        [NonSerialized]
        public bool IsDisplay = false;

        /// <summary>
        /// Is narrative previously displayed?
        /// </summary>
        [NonSerialized]
        public bool IsAlreadyDisplayed = false;
    }

    /// <summary>
    /// Game outcome (Alternative for score)
    /// </summary>
    [Serializable]
    public class EndResult
    {
        /// <summary>
        /// Display text on the screen
        /// </summary>
        [XmlElement("DisplayText")]
        public string DisplayText { get; set; }

        /// <summary>
        /// Position of text on the screen
        /// </summary>
        [XmlElement("TextPostion")]
        public string TextPostion { get; set; }
    }
}
