﻿using Microsoft.Xna.Framework;

namespace cga_coursework
{
    class DisplayTextData
    {

        /// <summary>
        /// Font Id
        /// </summary>
        private string m_FontId = null;
        public string FontId { get { return m_FontId; } }

        /// <summary>
        /// Position of the text on the screen
        /// </summary>
        public Vector2 Position { get; set; }
        //private Vector2 m_vec2Position = Vector2.Zero;
       // public Vector2 Position { get { return m_vec2Position; } }

        /// <summary>
        /// Text Color
        /// </summary>
        public Color Color { get; set; } = Color.White;

        /// <summary>
        /// Is Text visible on the screen?
        /// </summary>
        public bool IsVisible { get; set; } = false;

        /// <summary>
        /// Display string
        /// </summary>
        public string DisplayText { get; set; } = string.Empty;

        /// <summary>
        /// Size of the text
        /// </summary>
        private float m_fltScale = 1.0f;
        public float Scale { get { return m_fltScale; } }

        /// <summary>
        /// Contructor for unit scale
        /// </summary>
        /// <param name="a_FontId"></param>
        /// <param name="a_strDisplayText"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_color"></param>
        /// <param name="a_isVisible"></param>
        public DisplayTextData(string a_FontId, string a_strDisplayText, Vector2 a_vec2Position, Color a_color, bool a_isVisible = true)
        {
            m_FontId = a_FontId;
            Position = a_vec2Position;
            Color = a_color;
            IsVisible = a_isVisible;
            DisplayText = a_strDisplayText;
            m_fltScale = 1.0f;
        }

        /// <summary>
        /// Contructor for custom scale
        /// </summary>
        /// <param name="a_FontId"></param>
        /// <param name="a_strDisplayText"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_color"></param>
        /// <param name="a_fltScale"></param>
        /// <param name="a_isVisible"></param>
        public DisplayTextData(string a_FontId, string a_strDisplayText, Vector2 a_vec2Position, Color a_color, float a_fltScale, bool a_isVisible = true)
        {
            m_FontId = a_FontId;
            Position = a_vec2Position;
            Color = a_color;
            IsVisible = a_isVisible;
            DisplayText = a_strDisplayText;
            m_fltScale = a_fltScale;
        }

    }
}
