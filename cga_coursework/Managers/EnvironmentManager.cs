﻿using System;

namespace cga_coursework
{
    class EnvironmentManager
    {

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static EnvironmentManager s_instance = null;
        private static EnvironmentManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new EnvironmentManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private EnvironmentManager()
        {
            m_InteractableObjectsRef = new InteractableObjects();
            m_NonInteractbaleObjectsRef = new NonInteractbaleObjects();
        }

        /// <summary>
        /// Interactive object of environment (Player can interact) 
        /// </summary>
        private InteractableObjects m_InteractableObjectsRef = null;
        public static InteractableObjects InteractableObjects { get { return Instance.m_InteractableObjectsRef; } }

        /// <summary>
        /// Non-Interactive object of environment 
        /// </summary>
        private NonInteractbaleObjects m_NonInteractbaleObjectsRef = null;
        public static NonInteractbaleObjects NonInteractbaleObjects { get { return Instance.m_NonInteractbaleObjectsRef; } }

        /// <summary>
        /// Register event on interactive objects when player interact with it(interact = collided)
        /// </summary>
        /// <param name="a_event"></param>
        public static void RegisterEvent(Action<string> a_event)
        {
            Instance.m_InteractableObjectsRef.OnCollisionWithInteractiveObjects -= a_event;
            Instance.m_InteractableObjectsRef.OnCollisionWithInteractiveObjects += a_event;
        }

        /// <summary>
        /// Unregister event on interactive objects
        /// </summary>
        /// <param name="a_event"></param>
        public static void UnregisterEvent(Action<string> a_event)
        {
            Instance.m_InteractableObjectsRef.OnCollisionWithInteractiveObjects -= a_event;
        }

        /// <summary>
        /// For testing (is collider visible?)
        /// </summary>
        public static void ToggleEnvironmentColliders()
        {
            Instance.m_InteractableObjectsRef.ToggleColliders();
            Instance.m_NonInteractbaleObjectsRef.ToggleColliders();
        }
    }
}