﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace cga_coursework
{
    class GuidanceManager
    {

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static GuidanceManager s_instance = null;
        private static GuidanceManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new GuidanceManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private GuidanceManager()
        {

        }

        /// <summary>
        /// Guidance messages
        /// </summary>
        private const string MESSAGE_1 = "PRESS            / ENTER TO CONTINUE";
        private const string MESSAGE_2 = "PRESS            / SPACE TO OPEN";
        private const string MESSAGE_3 = "YOU HAVE THE DOCUMENTS";
        private const string MESSAGE_4 = "ENTER THE KEY TO OPEN THE DOOR";
        private const string MESSAGE_5 = "CHECK THE TABLE FOR RIGHT KEY";

        /// <summary>
        /// Controller A button texture
        /// </summary>
        private Texture2D m_texControllerA = null;

        /// <summary>
        /// Controller X button texture
        /// </summary>
        private Texture2D m_texControllerX = null;
        
        /// <summary>
        /// I button texture to indicate its a clue or something that is interactive
        /// </summary>
        private Texture2D m_texIBtn = null;

        /// <summary>
        /// Game object for controller buttons
        /// </summary>
        private GameObject m_objDisplayControllerImage;

        /// <summary>
        /// Gameobject for I button
        /// </summary>
        private GameObject m_objDisplayIBtnImage;

        /// <summary>
        /// Display guidance message
        /// </summary>
        private DisplayTextData m_currentDisplayText;

        /// <summary>
        /// Is guidance UI visible
        /// </summary>
        private bool m_isShowGuidance = false;

        /// <summary>
        /// Height of the screen
        /// </summary>
        private float Height { get; set; }

        /// <summary>
        /// Width of the screen
        /// </summary>
        private float Width { get; set; }

        /// <summary>
        /// Initiaize 
        /// </summary>
        /// <param name="a_texControllerA"></param>
        /// <param name="a_texControllerX"></param>
        /// <param name="a_texIBtn"></param>
        /// <param name="a_fltHeight"></param>
        /// <param name="a_width"></param>
        public static void Initialize(Texture2D a_texControllerA, Texture2D a_texControllerX, Texture2D a_texIBtn, float a_fltHeight, float a_width)
        {
            Instance.m_texControllerA = a_texControllerA;
            Instance.m_texControllerX = a_texControllerX;
            Instance.m_texIBtn = a_texIBtn;
            Instance.Height = a_fltHeight;
            Instance.Width = a_width;

            Instance.m_currentDisplayText = new DisplayTextData("", "", new Vector2(Instance.Width - 450.0f, Instance.Height - 80.0f), Color.White);
            HudManager.RegisterTextOnHud(Instance.m_currentDisplayText);
        }

        /// <summary>
        /// Show Guidance UI
        /// </summary>
        /// <param name="a_message"></param>
        public static void ShowGuidanceMessage(EGuidanceMessage a_message)
        {


            switch (a_message)
            {
                case EGuidanceMessage.None:
               
                    return;

                case EGuidanceMessage.Message1:

                    // IMAGE
                     Instance.m_objDisplayControllerImage = new GameObject(EGuidanceMessage.Message1.ToString(), new Vector2(Instance.Width-355.0f, Instance.Height - 80.0f), new Vector2(0.13f, 0.13f));
                     Instance.m_objDisplayControllerImage.Initialize(Instance.m_texControllerA);
                     Instance.m_objDisplayIBtnImage = null;

                    // TEXT
                    Instance.m_currentDisplayText.DisplayText = MESSAGE_1;

                    break;

                case EGuidanceMessage.Message2:

                    // IMAGE
                    Instance.m_objDisplayControllerImage = new GameObject(EGuidanceMessage.Message2.ToString(), new Vector2(Instance.Width - 360.0f, Instance.Height - 85.0f), new Vector2(0.08f, 0.08f));
                    Instance.m_objDisplayControllerImage.Initialize(Instance.m_texControllerX);

                    Instance.m_objDisplayIBtnImage = new GameObject(EGuidanceMessage.Message2.ToString(), new Vector2(Instance.Width - 535.0f, Instance.Height - 100.0f), new Vector2(0.7f, 0.7f));
                    Instance.m_objDisplayIBtnImage.Initialize(Instance.m_texIBtn);
                    // TEXT
                    Instance.m_currentDisplayText.DisplayText = MESSAGE_2;
                    break;
                case EGuidanceMessage.Message3:
                    // IMAGE
                    Instance.m_objDisplayControllerImage = null;
                    Instance.m_objDisplayIBtnImage = null;

                    // TEXT
                    Instance.m_currentDisplayText.DisplayText = MESSAGE_3;
                    break;
                case EGuidanceMessage.Message4:
                    // IMAGE
                    Instance.m_objDisplayControllerImage = null;
                    Instance.m_objDisplayIBtnImage = null;

                    // TEXT
                    Instance.m_currentDisplayText.DisplayText = MESSAGE_4;
                    break;
                case EGuidanceMessage.Message5:
                    // IMAGE
                    Instance.m_objDisplayControllerImage = null;
                    Instance.m_objDisplayIBtnImage = null;

                    // TEXT
                    Instance.m_currentDisplayText.DisplayText = MESSAGE_5;
                    break;
                default:
                    break;
            }

            Instance.m_isShowGuidance = true;
        }

        /// <summary>
        /// Hide Guidance UI
        /// </summary>
        public static void HideGuidanceMessage()
        {
            if (Instance.m_currentDisplayText != null)
            { 
            Instance.m_currentDisplayText.DisplayText = string.Empty;
                
            }

            Instance.m_isShowGuidance = false;
            Instance.m_objDisplayIBtnImage = null;
            Instance.m_objDisplayControllerImage = null;
        }

        /// <summary>
        /// Render Guidance UI
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void DrawGuidance(SpriteBatch a_spriteBatch)
        {
            if (!Instance.m_isShowGuidance)
            {
                return;
            }

            Instance.m_objDisplayControllerImage?.Draw(a_spriteBatch);
            Instance.m_objDisplayIBtnImage?.Draw(a_spriteBatch);
        }
    }
}
