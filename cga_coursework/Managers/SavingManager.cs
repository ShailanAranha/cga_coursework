﻿using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace cga_coursework
{
    class SavingManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static SavingManager s_instance = null;
        private static SavingManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new SavingManager();
                }


                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private SavingManager()
        {
        }

        /// <summary>
        /// Object that contains all the saved data from previous session
        /// </summary>
        public SavedData m_savedData = null;                                        
        public static SavedData SavedData
        {
            get
            {
                if (Instance.m_savedData == null)
                {
                    Instance.m_savedData = Instance.GetSavedData();
                }

                return Instance.m_savedData;
            }
        }

        /// <summary>
        /// Folder name where the dat file (binary file) is stored
        /// </summary>
        private const string FOLDER_NAME = @"\GameArchitecture\";

        /// <summary>
        /// File name of the dat file (That contains saved element in binary)
        /// </summary>
        private const string FILE_NAME = @"savedData.dat";

        /// <summary>
        /// Persistent data path of the device where dat files is stored
        /// </summary>
        private string GetPersistentDataPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + FOLDER_NAME + FILE_NAME;
            }
        }

        /// <summary>
        /// Persistent direectory of the device where dat files is stored
        /// </summary>
        private string GetPersistentDataDirectory
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + FOLDER_NAME;
            }
        }

        /// <summary>
        /// Does old session exits? (Is this game played for the first time in a device)
        /// </summary>
        public bool m_OldSessionExists = false;
        public static bool OldSessionExists 
        { 
            get
            {
                if (Instance.m_savedData == null)
                { 
                    SavedData l_temp = SavedData; // TO MAKE SURE THAT DATA IS LOADED
                }

                // return false;
                return Instance.m_OldSessionExists;
            }
        }

        /// <summary>
        /// Event inoked when the saved data is flushed (old session data is deleted)
        /// </summary>
        public static event Action OnDataFlushed = null;

        /// <summary>
        /// Save player's current position
        /// </summary>
        /// <param name="a_position"></param>
        public static void SavePlayerPosition(Vector2 a_position)
        {
            //Instance.m_savedData.PlayerCurrentPosition = a_position;
            Instance.m_savedData.X = a_position.X;
            Instance.m_savedData.Y = a_position.Y;
        }

        /// <summary>
        /// Save Game time
        /// </summary>
        /// <param name="a_dGameTime"></param>
        public static void SaveGameTime(double a_dGameTime)
        {
            Instance.m_savedData.GameTime = a_dGameTime;
        }

        /// <summary>
        /// Save if intro narrative is shown
        /// </summary>
        /// <param name="a_isNarrativeShnown"></param>
        public static void SaveIntroNarratibeData(bool a_isNarrativeShnown)
        {
            Instance.m_savedData.IsNarrativeShown = a_isNarrativeShnown;
        }

        /// <summary>
        /// Save if intro narrative is completed
        /// </summary>
        /// <param name="a_isNarrativeCompleted"></param>
        public static void SaveIntroNarrativeData(bool a_isNarrativeCompleted)
        {
            Instance.m_savedData.IsIntroNarrativeCompleted = a_isNarrativeCompleted;
        }

        /// <summary>
        /// Safe if safe is opened
        /// </summary>
        /// <param name="a_isSafeOpen"></param>
        public static void SaveSafeOpenData(bool a_isSafeOpen)
        {
            Instance.m_savedData.IsSafeOpened = a_isSafeOpen;
        }

        /// <summary>
        /// Save if door is opened
        /// </summary>
        /// <param name="a_isDoorOpen"></param>
        public static void SaveDoorOpenData(bool a_isDoorOpen)
        {
            Instance.m_savedData.IsDoorOpened = a_isDoorOpen;
        }

        /// <summary>
        /// Save if, in the old session the game is completed
        /// </summary>
        /// <param name="a_isOldSessionCompleted"></param>
        public static void SaveOldSessionData(bool a_isOldSessionCompleted)
        {
            Instance.m_savedData.IsOldSessionCompleted = a_isOldSessionCompleted;
        }

        /// <summary>
        /// Save data
        /// </summary>
        public static void SaveData()
        {
            Serialize();
        }

        /// <summary>
        /// Get saved data 
        /// </summary>
        /// <returns></returns>
        private SavedData GetSavedData()
        {
            SavedData l_savedData = null;

            //try
            //{
            //    l_savedData = Deserialize();
            //}
            //catch (Exception Ex)
            //{
            //    Console.WriteLine("EXCEPTION::SavingManager::" + Ex.Message);
            //    l_savedData = new SavedData();
            //}

            if (l_savedData == null)
            { 
                l_savedData = Deserialize();
            }

            return l_savedData;
        }

        /// <summary>
        /// Serilaize data in dat file
        /// </summary>
        private static void Serialize()
        {
            if (Instance.m_savedData == null)
            {
                return;
            }

            if (!Directory.Exists(Instance.GetPersistentDataDirectory))
            {
                Directory.CreateDirectory(Instance.GetPersistentDataDirectory);
            }

            // FileStream fs = new FileStream(@"P:\City\Computer Game Architecture\Coursework\SavedData\DataFile.dat", FileMode.Create, FileAccess.Write, FileShare.None);
            FileStream fs = new FileStream(Instance.GetPersistentDataPath, FileMode.Create, FileAccess.Write, FileShare.None);

            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, Instance.m_savedData);

            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                // throw;
            }
            finally
            {
                fs.Flush();
                fs.Close();
                fs.Dispose();
            }
        }

        /// <summary>
        /// Deserialize data from the dat file
        /// </summary>
        /// <returns></returns>
        private static SavedData Deserialize()
        {
            SavedData l_savedData = null;


            if (!File.Exists(Instance.GetPersistentDataPath))
            {
                Instance.m_OldSessionExists = false;
                l_savedData = new SavedData();
                return l_savedData;
            }

            Instance.m_OldSessionExists = true;

            // FileStream fs = new FileStream(@"P:\City\Computer Game Architecture\Coursework\SavedData\DataFile.dat", FileMode.Open);
            FileStream fs = new FileStream(Instance.GetPersistentDataPath, FileMode.Open);


            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                l_savedData = (SavedData)formatter.Deserialize(fs);

            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                l_savedData = new SavedData();
                //throw;
            }
            finally
            {
                fs.Flush();
                fs.Close();
                fs.Dispose();
            }


            return l_savedData;
        }

        /// <summary>
        /// Flush the saved data
        /// </summary>
        public static void FlushData()
        {
            Instance.m_savedData = new SavedData();
            OnDataFlushed?.Invoke();
        }
    }

    /// <summary>
    /// Game elements that needs to be saved 
    /// </summary>
    [System.Serializable]
    public class SavedData
    {
        /// <summary>
        /// Player position X
        /// </summary>
        public float X = 0.0f;
        
        /// <summary>
        /// Player position Y
        /// </summary>
        public float Y = 0.0f;

        /// <summary>
        /// Game timer (reverse clock)
        /// </summary>
        public double GameTime = 0.0;

        /// <summary>
        /// Is intro narrative shown?
        /// </summary>
        public bool IsNarrativeShown = false;

        /// <summary>
        /// Is intro narrative finish by the player? (check all pages of intro narrative)
        /// </summary>
        public bool IsIntroNarrativeCompleted = false;

        /// <summary>
        /// Is safe opened by the player? 
        /// </summary>
        public bool IsSafeOpened = false;

        /// <summary>
        /// Is door opend by the player? (NOT USED AT THE MOMENT)
        /// </summary>
        public bool IsDoorOpened = false;

        /// <summary>
        /// Is Game completed from previous session?
        /// </summary>
        public bool IsOldSessionCompleted = false;

        /// <summary>
        /// Get last saved player position (from previous session)
        /// </summary>
        public Vector2 PlayerCurrentPosition
        {
            get
            {
                return new Vector2(X, Y);
            }
        }

    }
}
