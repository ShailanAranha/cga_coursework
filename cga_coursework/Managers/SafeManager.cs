﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class SafeManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static SafeManager s_instance = null;
        private static SafeManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new SafeManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private SafeManager()
        {

        }

        /// <summary>
        /// Safe pin enter texture
        /// </summary>
        private Texture2D m_texSafe = null;
        public static Texture2D SafeTexture { get { return Instance.m_texSafe; } }

        /// <summary>
        /// Transperent background texture
        /// </summary>
        private Texture2D m_texBg = null;
        public static Texture2D BgTexture { get { return Instance.m_texBg; } }


        /// <summary>
        /// Safe gameobject (independent function that is used to render any texture on the screen)
        /// </summary>
        private GameObject m_goSafe = null;

        /// <summary>
        /// Safe gameobject 
        /// </summary>
        private GameObject m_imgTransparentBg = null;

        /// <summary>
        /// Display Pin text on the screen
        /// </summary>
        private DisplayTextData m_txtDisplayPin;

        /// <summary>
        /// Display if the pin entered is correct or incorrect
        /// </summary>
        private DisplayTextData m_txtPinFeedback;
        
        /// <summary>
        /// Pin string
        /// </summary>
        private string DisplayPin { get; set; } = "---";

        /// <summary>
        /// Current Pin index to be entered
        /// </summary>
        private int PinIndex { get; set; } = 0;

        /// <summary>
        /// Enbale user input to enter pin
        /// </summary>
        private bool EnableUserInput { get; set; } = false;

        /// <summary>
        /// Is safe UI visible?
        /// </summary>
        private bool IsShowSafeUI { get; set; } = false;

        /// <summary>
        /// Is safe already opened?
        /// </summary>
        private bool IsSafeOpened { get; set; } = false;

        /// <summary>
        /// Event invoked when the player enters all three-digit of the pin
        /// </summary>
        public static event Action<string> OnUserInputComplted = null;
        
        /// <summary>
        /// Event invoked when player enteres correct pin
        /// </summary>
        public static event Action OnSuccessfullPinEntry = null;

        /// <summary>
        /// Height of scrren 
        /// </summary>
        private float Height = 0.0f;

        /// <summary>
        /// Width of the scrren
        /// </summary>
        private float Width = 0.0f;

        /// <summary>
        /// Initialize data
        /// </summary>
        /// <param name="a_texSafe"></param>
        /// <param name="a_texBg"></param>
        /// <param name="a_fltWidth"></param>
        /// <param name="a_fltHeight"></param>
        public static void Initialize(Texture2D a_texSafe, Texture2D a_texBg, float a_fltWidth, float a_fltHeight)
        {
            Instance.m_texSafe = a_texSafe;
            Instance.m_texBg = a_texBg;
            Instance.Height = a_fltHeight;
            Instance.Width = a_fltWidth;

            OnUserInputComplted -= VerifyPin;
            OnUserInputComplted += VerifyPin;

            Instance.IsSafeOpened = SavingManager.SavedData.IsSafeOpened;
            SavingManager.OnDataFlushed += () =>
            {
                Instance.IsSafeOpened = false;
                SavingManager.SaveSafeOpenData(false);
                EnvironmentManager.RegisterEvent(DisplaySafeUI);
            };

        }

        /// <summary>
        /// Display Guidance message for player when collider with safe object
        /// </summary>
        /// <param name="a_strInteractiveObjectId"></param>
        public static void DisplaySafeUI(string a_strInteractiveObjectId)
        {
            if (InteractableObjects.Safe_ID.Equals(a_strInteractiveObjectId))
            {
                if (Instance.IsSafeOpened)
                {
                    GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message3);
                }
                else
                { 
                ShowSafeUI();
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);
                }
            }
        }

        /// <summary>
        /// Show safe UI
        /// </summary>
        public static void ShowSafeUI()
        {
            if (Instance.m_goSafe == null)
            { 
                Instance.m_goSafe = new GameObject("Safe", new Vector2(ScreenDimensionManager.ScreenDimensions.X/2 - 200, ScreenDimensionManager.ScreenDimensions.Y / 2 - 200), new Vector2(0.6f, 0.6f));
                Instance.m_goSafe.Initialize(Instance.m_texSafe);

                Instance.m_imgTransparentBg = new GameObject("TrasparentBg", Vector2.Zero, new Vector2(50, 50));
                Instance.m_imgTransparentBg.Initialize(Instance.m_texBg);
            }

            if (Instance.m_txtDisplayPin == null)
            {
                Instance.m_txtDisplayPin = new DisplayTextData("", "", new Vector2(Instance.Width / 2 - 85, Instance.Height / 2 - 160), Color.Green, 3.0f);
                HudManager.RegisterTextOnHud(Instance.m_txtDisplayPin);
            }

            if (Instance.m_txtPinFeedback == null)
            {
                Instance.m_txtPinFeedback = new DisplayTextData("", "", new Vector2(Instance.Width / 2 -350, Instance.Height / 2 + 300), Color.White, 3.0f);
                HudManager.RegisterTextOnHud(Instance.m_txtPinFeedback);
            }


            Instance.DisplayPin = "---";
            Instance.m_txtDisplayPin.DisplayText = Instance.DisplayPin;


            Instance.EnableUserInput = true;
            Instance.IsShowSafeUI = true;

            PlayerManager.SetPlayerMovement(false);

            InputManager.OnPressedContinueBtn -= HideSafeUI;
            InputManager.OnPressedContinueBtn += HideSafeUI;

            AudioManager.StopSFX(GameAudio.OPEN_CLUE);
            AudioManager.PlaySFX(GameAudio.OPEN_CLUE);
        }

        /// <summary>
        /// Hide safe UI
        /// </summary>
        public static void HideSafeUI()
        {
            Instance.EnableUserInput = false;
            Instance.IsShowSafeUI = false;
            Instance.DisplayPin = string.Empty;
            Instance.m_txtDisplayPin.DisplayText = Instance.DisplayPin;
            Instance.m_txtPinFeedback.DisplayText = string.Empty;
            Instance.PinIndex = 0;

            PlayerManager.SetPlayerMovement(true);

            InputManager.OnPressedContinueBtn -= HideSafeUI;

            if (Instance.IsSafeOpened)
            { 
                MainGameManager.ShowPostSafeOpenNarrative();
            }
        }


        /// <summary>
        /// player input for the pIN
        /// </summary>
        /// <param name="a_num"></param>
        public static void UserInput(char a_num)
        {
            //char[] l_arrPin = Instance.DisplayPin.ToCharArray();

            //for (int i = 0; i < l_arrPin.Length; i++)
            //{
            //    if (l_arrPin[i].Equals('-'))
            //    {
            //        l_arrPin[i] = a_num;
            //    }
            //}
            if (!Instance.EnableUserInput)
            {
                return;
            }
            
            Instance.m_txtPinFeedback.DisplayText = string.Empty;

            char[] l_arr = Instance.DisplayPin.ToCharArray();
            l_arr[Instance.PinIndex] = a_num;
            Instance.DisplayPin = new string(l_arr) ;
            //Instance.DisplayPin.ToCharArray()[Instance.PinIndex] = a_num;
            //Instance.DisplayPin
            Instance.m_txtDisplayPin.DisplayText = Instance.DisplayPin;
            
            if (Instance.PinIndex == 2)
            {
                Instance.PinIndex = 0;
                Instance.EnableUserInput = false;
                OnUserInputComplted?.Invoke(Instance.DisplayPin);
            }
            else
            { 
                ++Instance.PinIndex;
            }

            AudioManager.StopSFX(GameAudio.TYPING_PIN);
            AudioManager.PlaySFX(GameAudio.TYPING_PIN);

        }

        /// <summary>
        /// Verifies if the pin entered matches with the one in XML
        /// </summary>
        /// <param name="a_strPin"></param>
        public static void VerifyPin(string a_strPin)
        {
            if (GameDataManager.GameData.Pin.Equals(a_strPin))
            {
                Instance.m_txtPinFeedback.DisplayText = ">>Correct PIN<<";
                Instance.m_txtPinFeedback.Color = Color.Green;
                Instance.IsSafeOpened = true;
                AudioManager.StopSFX(GameAudio.CORRECT_PIN);
                AudioManager.PlaySFX(GameAudio.CORRECT_PIN);
                SavingManager.SaveSafeOpenData(Instance.IsSafeOpened);
                OnSuccessfullPinEntry?.Invoke();
            }
            else
            {
                Instance.m_txtPinFeedback.DisplayText = ">>Incorrect PIN<<";
                Instance.m_txtPinFeedback.Color = Color.Red;
                AudioManager.StopSFX(GameAudio.INCORRECT_PIN);
                AudioManager.PlaySFX(GameAudio.INCORRECT_PIN);
                ResetPin();
            }
        }

        /// <summary>
        /// Reset pin for another try
        /// </summary>
        private static void ResetPin()
        {
            Instance.DisplayPin = "---";
            Instance.m_txtDisplayPin.DisplayText = Instance.DisplayPin;
            Instance.PinIndex = 0;
            Instance.EnableUserInput = true;
        }

        /// <summary>
        /// Game loop
        /// </summary>
        public static void Update()
        { 
        
        }

        /// <summary>
        /// Render safe UI (PIN enter) on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void Draw(SpriteBatch a_spriteBatch)
        {
            if (!Instance.IsShowSafeUI)
            {
                return;
            }

            Instance.m_imgTransparentBg.Draw(a_spriteBatch);
            Instance.m_goSafe.Draw(a_spriteBatch);
        }
    }
}
