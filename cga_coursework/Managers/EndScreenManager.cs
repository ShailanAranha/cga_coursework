﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace cga_coursework
{
    class EndScreenManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static EndScreenManager s_instance = null;
        private static EndScreenManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new EndScreenManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private EndScreenManager()
        {
        }

        /// <summary>
        /// End screen background 
        /// </summary>
        private GameObject m_objBg = null;

        /// <summary>
        /// End screen buttons
        /// </summary>
        private Button m_btnRestart = null;
        private Button m_btnQuit = null;

        /// <summary>
        /// Is End screen visible?
        /// </summary>
        private bool m_isEndScreenActive = false;

        /// <summary>
        /// Current button selected
        /// </summary>
        private EEndScreenOptions m_ECurrentSelectedBtn = EEndScreenOptions.Restart;

        /// <summary>
        /// Event invoked when quit button is clicked
        /// </summary>
        public static event Action onClickQuit = null;

        /// <summary>
        /// End game content ids
        /// </summary>
        private const string BG_TEXTURE_ID = "Start_screen_bg";
        private const string RESTART_BTN_ID = "restart_btn";
        private const string QUIT_BTN_ID = "quit_btn";

        /// <summary>
        /// End result text (render)
        /// </summary>
        private DisplayTextData m_endscreenDisplayText;

        /// <summary>
        /// End result text
        /// </summary>
        private string EndScreenMessage = null;
        
        /// <summary>
        /// End screen result text position
        /// </summary>
        private Vector2 EndScreenMessagePos = Vector2.Zero;

        /// <summary>
        /// Load data on End screen UI
        /// </summary>
        /// <param name="a_content"></param>
        public static void LoadData(ContentManager a_content)
        {
            Initialize(a_content.Load<Texture2D>(@"Graphics\EndScreen\bg"),
                          a_content.Load<Texture2D>(@"Graphics\EndScreen\restart_normal"), a_content.Load<Texture2D>(@"Graphics\EndScreen\restart_highlighted"),
                          a_content.Load<Texture2D>(@"Graphics\StartScreen\quit_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\quit_highlighted"));

        }

        /// <summary>
        /// Initialize End Screen properites
        /// </summary>
        /// <param name="a_texBg"></param>
        /// <param name="a_texRestartNormal"></param>
        /// <param name="a_texRestartHighlighted"></param>
        /// <param name="a_texQuitNormal"></param>
        /// <param name="a_texQuitHighlighted"></param>
        private static void Initialize(Texture2D a_texBg, Texture2D a_texRestartNormal, Texture2D a_texRestartHighlighted,
            Texture2D a_texQuitNormal, Texture2D a_texQuitHighlighted)
        {
            Instance.m_objBg = new GameObject(BG_TEXTURE_ID, Vector2.Zero, false);
            Instance.m_objBg.Initialize(a_texBg);

            Vector2 l_vec2ContinuePos = Vector2.Zero;
            Vector2 l_vec2PlayPos = Vector2.Zero;

            float l_fltHalfWidth = ScreenDimensionManager.ScreenDimensions.X / 2 - 200;
            float l_fltHalfHeight = ScreenDimensionManager.ScreenDimensions.Y / 2;

            l_vec2ContinuePos = new Vector2(l_fltHalfWidth, l_fltHalfHeight - 100);
            l_vec2PlayPos = new Vector2(l_fltHalfWidth, l_fltHalfHeight);

            Instance.m_btnRestart = new Button(RESTART_BTN_ID, l_vec2ContinuePos, new Vector2(0.5f, 0.5f), a_texRestartNormal, a_texRestartHighlighted);
            Instance.m_btnQuit = new Button(QUIT_BTN_ID, l_vec2PlayPos, new Vector2(0.5f, 0.5f), a_texQuitNormal, a_texQuitHighlighted);

            //Instance.m_btnRestart.OnClick += Instance.OnClickRestartBtn;
            //Instance.m_btnQuit.OnClick += Instance.OnClickExitBtn;

        }

        /// <summary>
        /// Unload End game contents 
        /// </summary>
        public static void UnloadContent()
        {
            Instance.m_objBg.Texture.Dispose();
            Instance.m_btnRestart.Texture.Dispose();
            Instance.m_btnQuit.Texture.Dispose();
        }

        /// <summary>
        /// Show End Game 
        /// </summary>
        public static void ShowUI()
        {
            Instance.m_btnRestart.OnClick += Instance.OnClickRestartBtn;
            Instance.m_btnQuit.OnClick += Instance.OnClickExitBtn;

            InputManager.OnPressedUp += Instance.SwitchBtnUp;
            InputManager.OnPressedDown += Instance.SwitchBtnDown;
            Instance.m_isEndScreenActive = true;


            float l_fltScreenWidth = ScreenDimensionManager.ScreenDimensions.X;
            float l_fltScreenHeight = ScreenDimensionManager.ScreenDimensions.Y;
            Instance.m_endscreenDisplayText = new DisplayTextData(string.Empty, string.Empty, new Vector2(l_fltScreenWidth / 2 - 250, l_fltScreenHeight / 2-250), Color.Crimson);
            HudManager.RegisterTextOnHud(Instance.m_endscreenDisplayText);

            AudioManager.PlayAudio(GameAudio.INTRO_GAME);
        }

        /// <summary>
        /// Hide end game
        /// </summary>
        public static void HideUI()
        {
            Instance.m_isEndScreenActive = false;
            InputManager.OnPressedUp -= Instance.SwitchBtnUp;
            InputManager.OnPressedDown -= Instance.SwitchBtnDown;

            Instance.m_btnRestart.OnClick -= Instance.OnClickRestartBtn;
            Instance.m_btnQuit.OnClick -= Instance.OnClickExitBtn;

            Instance.m_endscreenDisplayText.DisplayText = string.Empty;
            HudManager.UnregisterTextOnHud(Instance.m_endscreenDisplayText);
            Instance.EndScreenMessage = string.Empty;

            AudioManager.StopAudio();
        }

        /// <summary>
        /// Navigate down for end screen buttons
        /// </summary>
        private void SwitchBtnDown()
        {
            if (Instance.m_ECurrentSelectedBtn.Equals(EEndScreenOptions.Restart))
            {
                Instance.m_ECurrentSelectedBtn = EEndScreenOptions.Quit;
            }
            else
            {
                Instance.m_ECurrentSelectedBtn = EEndScreenOptions.Restart;
            }

            AudioManager.StopSFX(GameAudio.BUTTON_TRANSITION);
            AudioManager.PlaySFX(GameAudio.BUTTON_TRANSITION);
        }

        /// <summary>
        /// Navigate up for end screen buttons
        /// </summary>
        private void SwitchBtnUp()
        {
            if (Instance.m_ECurrentSelectedBtn.Equals(EEndScreenOptions.Restart))
            {
                Instance.m_ECurrentSelectedBtn = EEndScreenOptions.Quit;
            }
            else
            { 
                Instance.m_ECurrentSelectedBtn = EEndScreenOptions.Restart;
            }

            AudioManager.StopSFX(GameAudio.BUTTON_TRANSITION);
            AudioManager.PlaySFX(GameAudio.BUTTON_TRANSITION);
        }

        /// <summary>
        /// On click restart button
        /// </summary>
        public void OnClickRestartBtn()
        {
            SavingManager.FlushData();
           // SavingManager.SaveOldSessionData(true);
            FSMGameManager.ExitGameState(EGameState.EndScreen);
            FSMGameManager.EnterGameState(EGameState.MainGame);

            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);
        }

        /// <summary>
        /// On Click Exit button 
        /// </summary>
        public void OnClickExitBtn()
        {
            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);

            SavingManager.FlushData();
            SavingManager.SaveOldSessionData(true);

            onClickQuit?.Invoke();
        }

        /// <summary>
        /// Get End Screen Result Message and its Position
        /// </summary>
        /// <param name="a_strEndScreenMessage"></param>
        /// <param name="a_vec2MessagePosition"></param>
        private void GetEndScreenMessageNPosition(out string a_strEndScreenMessage, out Vector2 a_vec2MessagePosition)
        {
            bool l_isDoorOpened = SavingManager.SavedData.IsDoorOpened;
            bool l_isSafeOpened = SavingManager.SavedData.IsSafeOpened;

            string[] l_arrDataPos = null;

            if (l_isDoorOpened && l_isSafeOpened)
            {
                a_strEndScreenMessage =  GameDataManager.GameData.YES_PIN_YES_KEY.DisplayText;
                l_arrDataPos = GameDataManager.GameData.YES_PIN_YES_KEY.TextPostion.Split(',');
              
            }
            else if (!l_isDoorOpened && l_isSafeOpened)
            {
                a_strEndScreenMessage = GameDataManager.GameData.YES_PIN_NO_KEY.DisplayText;
                l_arrDataPos = GameDataManager.GameData.YES_PIN_NO_KEY.TextPostion.Split(',');
            }
            else if (l_isDoorOpened && !l_isSafeOpened)
            {
                a_strEndScreenMessage = GameDataManager.GameData.NO_PIN_YES_KEY.DisplayText;
                l_arrDataPos = GameDataManager.GameData.NO_PIN_YES_KEY.TextPostion.Split(',');
            }
            else if (!l_isDoorOpened && !l_isSafeOpened)
            {
                a_strEndScreenMessage = GameDataManager.GameData.NO_PIN_NO_KEY.DisplayText;
                l_arrDataPos = GameDataManager.GameData.NO_PIN_NO_KEY.TextPostion.Split(',');
            }
            else
            {
                a_strEndScreenMessage = "Result not found";
                a_vec2MessagePosition = Vector2.Zero;
                return;
            }

            float l_fltXPosition = 0.0f;
            float l_fltYPosition = 0.0f;

            if (!float.TryParse(l_arrDataPos[0], out l_fltXPosition))
            {
                ; // TRY PARSE FAILED
            }

            if (!float.TryParse(l_arrDataPos[1], out l_fltYPosition))
            {
                ;// TRY PARSE FAILED
            }

            Vector2 l_vec2DisplayPosition = new Vector2(l_fltXPosition, l_fltYPosition);


            a_vec2MessagePosition = l_vec2DisplayPosition;
        }

        /// <summary>
        /// Game loop to update end game elements
        /// </summary>
        /// <param name="a_gameTime"></param>
        public static void Update(GameTime a_gameTime)
        {
            if (!Instance.m_isEndScreenActive)
            {
                return;
            }

            Instance.m_objBg.Update(a_gameTime);
            Instance.m_btnRestart.Update(a_gameTime);
            Instance.m_btnQuit.Update(a_gameTime);
        }

        /// <summary>
        /// Draw end game on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void Draw(SpriteBatch a_spriteBatch)
        {
            if (!Instance.m_isEndScreenActive)
            {
                return;
            }


            switch (Instance.m_ECurrentSelectedBtn)
            {
                case EEndScreenOptions.Restart:
                    Instance.m_btnRestart.SetHighlightedState();
                    Instance.m_btnQuit.SetNormalState();
                    break;
                case EEndScreenOptions.Quit:
                    Instance.m_btnRestart.SetNormalState();
                    Instance.m_btnQuit.SetHighlightedState();
                    break;
                default:
                    break;
            }

            Instance.m_objBg.Draw(a_spriteBatch);
            Instance.m_btnRestart.Draw(a_spriteBatch);
            Instance.m_btnQuit.Draw(a_spriteBatch);

            HudManager.DrawText(a_spriteBatch);

            if (string.IsNullOrEmpty(Instance.EndScreenMessage))
            {
               Instance.GetEndScreenMessageNPosition(out Instance.EndScreenMessage, out Instance.EndScreenMessagePos); // TO ENSURE TIS METHOD IS ONLY CALLED ONLY (OPTIMIZATION)
            }
            Instance.m_endscreenDisplayText.DisplayText = Instance.EndScreenMessage;
            Instance.m_endscreenDisplayText.Position = Instance.EndScreenMessagePos;

        }

    }
}
