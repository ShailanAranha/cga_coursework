﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class KeyManager
    {

        // <summary>
        /// Singleton Instance
        /// </summary>
        private static KeyManager s_instance = null;
        private static KeyManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new KeyManager();
                }


                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private KeyManager()
        {

        }

        /// <summary>
        /// User input towards key
        /// </summary>
        private char[] m_arrUserInput = new char[] {'-','-' };

        /// <summary>
        /// Current key index to be entered
        /// </summary>
        private int KeyIndex { get; set; } = 0;

        /// <summary>
        /// Enbale user input to enter key
        /// </summary>
        private bool EnableUserInput { get; set; } = false;

        /// <summary>
        /// Is door opened?
        /// </summary>
        private bool IsDoorOpened { get; set; } = false;

        /// <summary>
        /// vent invoked when the player enters all two-digit of the key
        /// </summary>
        public static event Action<string> OnUserInputComplted = null;

        /// <summary>
        /// Event invoked when player enteres correct key
        /// </summary>
        public static event Action OnSuccessfullKeyEntry = null;

        /// <summary>
        /// Initialize data
        /// </summary>
        public static void Initialize()
        {

            Instance.IsDoorOpened = SavingManager.SavedData.IsSafeOpened;
            SavingManager.OnDataFlushed += () =>
            {
                Instance.IsDoorOpened = false;
                SavingManager.SaveDoorOpenData(false);
            };

        }

        /// <summary>
        /// Enable key UI
        /// </summary>
        /// <param name="a_strInteractiveObjectId"></param>
        public static void EnableKey(string a_strInteractiveObjectId)
        {
            if (a_strInteractiveObjectId.Equals(InteractableObjects.Door_ID))
            {
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message4);
                PlayerManager.SetPlayerMovement(false);
                Instance.EnableUserInput = true;
                OnUserInputComplted -= VerifyPin;
                OnUserInputComplted += VerifyPin;

                AudioManager.StopSFX(GameAudio.OPEN_CLUE);
                AudioManager.PlaySFX(GameAudio.OPEN_CLUE);
            }
        }

        /// <summary>
        /// Display key UI
        /// </summary>
        public static void DisableKey()
        {
            PlayerManager.SetPlayerMovement(true);
            Instance.EnableUserInput = false;
            // GuidanceManager.HideGuidanceMessage();
            //GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message5);
        }

        /// <summary>
        /// player input for the key
        /// </summary>
        /// <param name="a_num"></param>
        public static void UserInput(char a_num)
        {
            if (!Instance.EnableUserInput)
            {
                return;
            }

            Instance.m_arrUserInput[Instance.KeyIndex] = a_num;

            if (Instance.KeyIndex == 1)
            {
                Instance.KeyIndex = 0;
                Instance.EnableUserInput = false;
                OnUserInputComplted?.Invoke(new string(Instance.m_arrUserInput));
            }
            else
            {
                ++Instance.KeyIndex;
            }

            AudioManager.StopSFX(GameAudio.TYPING_PIN);
            AudioManager.PlaySFX(GameAudio.TYPING_PIN);
        }

        /// <summary>
        /// Verifies if the key entered matches with the one in XML
        /// </summary>
        /// <param name="a_strKey"></param>
        public static void VerifyPin(string a_strKey)
        {
            if (GameDataManager.GameData.Key.Equals(a_strKey))
            {
                Instance.IsDoorOpened = true;
                SavingManager.SaveDoorOpenData(Instance.IsDoorOpened);
                OnSuccessfullKeyEntry?.Invoke();
                DisableKey();
            }
            else
            {
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message5);
                ResetPin();
                DisableKey();
            }
        }

        /// <summary>
        /// Reset key for another try
        /// </summary>
        private static void ResetPin()
        {
            Instance.m_arrUserInput[0] = '-';
            Instance.m_arrUserInput[1] = '-';
            Instance.KeyIndex = 0;
            Instance.EnableUserInput = true;
        }
    }
}
