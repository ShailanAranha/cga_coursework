﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace cga_coursework
{
    class PlayerManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static PlayerManager s_instance = null;
        private static PlayerManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new PlayerManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Represents player
        /// </summary>
        private Player m_player = null;

        /// <summary>
        /// Player instance
        /// </summary>
        public static Player Player { get { return Instance.m_player; } }

        /// <summary>
        /// Is there a player input
        /// </summary>
        private bool m_isInput = false;

        /// <summary>
        /// A movement speed for the layer
        /// </summary>
        // private const float PLAYER_MOVEMENT_SPEED = 8.0f;
        private const float PLAYER_MOVEMENT_SPEED = 0.5f;

        /// <summary>
        /// Wall size
        /// </summary>
        public const float WALL_OFFSET = 50.0f;

        /// <summary>
        /// Should player move?
        /// </summary>
        private bool m_isPlayerMovement = true;

        /// <summary>
        /// To make the player visible
        /// </summary>
        private bool m_isFirstIterationCompleted = false;


        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private PlayerManager()
        { 
        
        }

        /// <summary>
        /// Initialize
        /// </summary>
        public static void Initialize()
        {

            Instance.m_player = new Player();

            // SavingManager.OnDataFlushed += ()=> { Instance.m_isFirstIterationCompleted = false; };
            SavingManager.OnDataFlushed += () =>
            {
                Player.SetPlayerToStartPosition();
                Player.m_currentMovement = EPlayerMOvement.Down;
                Instance.m_isFirstIterationCompleted = false;
            };

        }

        /// <summary>
        /// Load data to player instance
        /// </summary>
        /// <param name="a_textIdle"></param>
        /// <param name="a_playerRight"></param>
        /// <param name="a_animLeft"></param>
        /// <param name="a_animUp"></param>
        /// <param name="a_animDown"></param>
        /// <param name="a_playerPosition"></param>
        public static void LoadData(Texture2D a_textIdle, Animation a_playerRight, Animation a_animLeft, Animation a_animUp, Animation a_animDown, Vector2 a_playerPosition)
        {
            //Instance.m_player.Initialize(a_textIdle, a_playerAnimation, a_playerPosition);
            Instance.m_player.Initialize(a_textIdle, a_playerRight, a_animLeft, a_animUp, a_animDown, a_playerPosition);

        }

        /// <summary>
        /// Draw player on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void Draw(SpriteBatch a_spriteBatch)
        {
            Instance.m_player.Draw(a_spriteBatch);
        }

        /// <summary>
        /// For texting
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void DrawCollider(SpriteBatch a_spriteBatch, GraphicsDevice a_graphicDevice)
        {
            Instance.m_player.DrawCollider(a_spriteBatch, a_graphicDevice);
        }

        /// <summary>
        /// Update for player components (Helper function)
        /// </summary>
        /// <param name="a_gameTime"></param>
        public static void UpdatePlayer(GameTime a_gameTime, GraphicsDevice a_GraphicsDevice)
        {
            if (!Instance.m_isPlayerMovement)
            {
                return;
            }

            // Instance.m_player.Update(a_gameTime);


            // get thumstick controls
            //Instance.m_player.CurrentPosition.X += InputManager.CurrentGamePadState.ThumbSticks.Left.X * PLAYER_MOVEMENT_SPEED;
            // Instance.m_player.CurrentPosition.Y -= InputManager.CurrentGamePadState.ThumbSticks.Left.Y * PLAYER_MOVEMENT_SPEED;


            // Use the keyboard / Dpad

            float l_fltDeltaTime = (float)a_gameTime.ElapsedGameTime.TotalMilliseconds;

            if (InputManager.CurrentKeyboardState.IsKeyDown(Keys.Left) || InputManager.CurrentGamePadState.DPad.Left == ButtonState.Pressed )
            {

                Instance.m_isInput = true;
                //Instance.m_player.IsIdle = false;
                // Instance.m_player.CurrentPosition.X -= PLAYER_MOVEMENT_SPEED;
                Instance.m_player.CurrentPosition.X -= PLAYER_MOVEMENT_SPEED * l_fltDeltaTime;


                Instance.m_player.m_currentMovement = EPlayerMOvement.Left;
                Instance.m_player.PlayerLeftWalkAnim.m_isActive = true;
            }

            if (InputManager.CurrentKeyboardState.IsKeyDown(Keys.Right) || InputManager.CurrentGamePadState.DPad.Right == ButtonState.Pressed)
            {

                Instance.m_isInput = true;
                //Instance.m_player.IsIdle = false;
                Instance.m_player.CurrentPosition.X += PLAYER_MOVEMENT_SPEED * l_fltDeltaTime;

                Instance.m_player.m_currentMovement = EPlayerMOvement.Right;
                Instance.m_player.PlayerRightWalkAnim.m_isActive = true;
            }

            if (InputManager.CurrentKeyboardState.IsKeyDown(Keys.Up) || InputManager.CurrentGamePadState.DPad.Up == ButtonState.Pressed)
            {

                Instance.m_isInput = true;
               // Instance.m_player.IsIdle = false;
                Instance.m_player.CurrentPosition.Y -= PLAYER_MOVEMENT_SPEED * l_fltDeltaTime;

                Instance.m_player.m_currentMovement = EPlayerMOvement.Up;
                Instance.m_player.PlayerUpWalkAnim.m_isActive = true;
            }

            if (InputManager.CurrentKeyboardState.IsKeyDown(Keys.Down) || InputManager.CurrentGamePadState.DPad.Down == ButtonState.Pressed)
            {
                Instance.m_isInput = true;
               // Instance.m_player.IsIdle = false;
                Instance.m_player.CurrentPosition.Y += PLAYER_MOVEMENT_SPEED * l_fltDeltaTime;

                Instance.m_player.m_currentMovement = EPlayerMOvement.Down;
                Instance.m_player.PlayerDownWalkAnim.m_isActive = true;

            }

            if (!Instance.m_isInput)
            {
                Instance.m_player.PlayerRightWalkAnim.m_isActive = false;
                Instance.m_player.PlayerLeftWalkAnim.m_isActive = false;
                Instance.m_player.PlayerUpWalkAnim.m_isActive = false;
                Instance.m_player.PlayerDownWalkAnim.m_isActive = false;
            }

            if (!Instance.m_isFirstIterationCompleted)
            {
                Instance.m_player.PlayerDownWalkAnim.m_isActive = true;
                Instance.m_isFirstIterationCompleted = true;
            }

            Instance.m_isInput = false; //   VALUE RESET

            // RO MAKE SURE THE PLAYER DOESN;T GO OUT OF BOUNDS

            Instance.m_player.CurrentPosition.X = MathHelper.Clamp(Instance.m_player.CurrentPosition.X, Instance.m_player.Width / 2 + WALL_OFFSET +10, a_GraphicsDevice.Viewport.Width - 10 - WALL_OFFSET);
            Instance.m_player.CurrentPosition.Y = MathHelper.Clamp(Instance.m_player.CurrentPosition.Y, Instance.m_player.Height / 2 + WALL_OFFSET+30, a_GraphicsDevice.Viewport.Height - Instance.m_player.Height / 2 - WALL_OFFSET);

            Instance.m_player.Update(a_gameTime);

            SavingManager.SavePlayerPosition(Instance.m_player.CurrentPosition);
        }

        /// <summary>
        /// Should player move?
        /// </summary>
        /// <param name="a_value"></param>
        public static void SetPlayerMovement(bool a_value)
        {
            Instance.m_isPlayerMovement = a_value;
        }

        /// <summary>
        /// For testing (is collider visible?)
        /// </summary>
        public static void ToggleColliders()
        {
            Instance.m_player.IsCollidableVisible = !Instance.m_player.IsCollidableVisible;
        }
       
    }
}
