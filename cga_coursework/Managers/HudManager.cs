﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace cga_coursework
{
    class HudManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static HudManager s_instance = null;
        private static HudManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new HudManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// The font used to display UI elements
        /// </summary>
        private SpriteFont m_font;
        public static SpriteFont Font { get { return Instance.m_font; } }

        /// <summary>
        /// Collection of display text to be rendered
        /// </summary>
        private List<DisplayTextData> m_lstDisplayText = null;

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private HudManager()
        {
            m_lstDisplayText = new List<DisplayTextData>(); // add the size of the list one number of hud elements are finilized
        }

        /// <summary>
        /// Load font
        /// </summary>
        /// <param name="a_font"></param>
        public static void LoadFont(SpriteFont a_font)
        {
            Instance.m_font = a_font; // Could be a list here
        }

        /// <summary>
        /// Register text to collection
        /// </summary>
        /// <param name="a_FontData"></param>
        public static void RegisterTextOnHud(DisplayTextData a_FontData)
        {
            Instance.m_lstDisplayText.Add(a_FontData);
        }

        /// <summary>
        /// Unegister text to collection
        /// </summary>
        /// <param name="a_FontData"></param>
        public static void UnregisterTextOnHud(DisplayTextData a_FontData)
        {
            //if (!Instance.m_lstDisplayText.Contains(a_FontData))
            //{
            //    // SHOW ERROR MESSAGE MAYBE WINDOW MESSAGE BOX POP UP
            //    return;
            //}

            Instance.m_lstDisplayText.Remove(a_FontData);
        }

        /// <summary>
        /// Draw all the text from the collection
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void DrawText(SpriteBatch a_spriteBatch)
        {
            for (int i = 0; i < Instance.m_lstDisplayText.Count; i++)
            {
                if (s_instance.m_lstDisplayText[i].IsVisible)
                { 
                    // a_spriteBatch.DrawString(Instance.m_font, Instance.m_lstDisplayText[i].DisplayText, Instance.m_lstDisplayText[i].Position, Instance.m_lstDisplayText[i].Color);
              
                     a_spriteBatch.DrawString(Instance.m_font, Instance.m_lstDisplayText[i].DisplayText, Instance.m_lstDisplayText[i].Position, Instance.m_lstDisplayText[i].Color, 0.0f, Vector2.Zero, s_instance.m_lstDisplayText[i].Scale, SpriteEffects.None, 1.0f);
                }
            }
        }
    }
}
