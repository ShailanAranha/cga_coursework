﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace cga_coursework
{
    class GameDataManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static GameDataManager s_instance = null;
        private static GameDataManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new GameDataManager();
                }

                return s_instance;
            }
        }


        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private GameDataManager()
        {
        }

        /// <summary>
        /// Game data objects that contains all game data
        /// </summary>
        private GameData m_gameData = null;
        public static GameData GameData
        {
            get
            {
                if (Instance.m_gameData == null)
                {
                    Instance.m_gameData = Instance.LoadData();
                    AddCluesAndNarrativeDataInList();
                }
                return Instance.m_gameData;
            }
        }

        /// <summary>
        /// Collection of textures for clues
        /// </summary>
        private List<TextureData> m_lstTextureData = null;
        public static IReadOnlyList<TextureData> LstTextureData { get { return Instance.m_lstTextureData; } }

        /// <summary>
        /// Collection of all the game clues (Key and pin)
        /// </summary>
        private List<Clue> m_lstClueData = null;
        public static IReadOnlyList<Clue> LstClueData { get { return Instance.m_lstClueData; } }

        /// <summary>
        /// Collection of all the narratives 
        /// </summary>
        private List<NarrativePage> m_lstNarrativeData = null;
        public static IReadOnlyList<NarrativePage> LstNarrativeData { get { return Instance.m_lstNarrativeData; } }

        /// <summary>
        /// Narartive background texture
        /// </summary>
        private Texture2D m_texNarrativeBg = null;
        public static Texture2D NarrativeBg { get { return Instance.m_texNarrativeBg; } }

        /// <summary>
        /// Game data file path (XML file,GameDataBase.xml)
        /// </summary>
        private const string GAME_DATA_FILE_EXTENSION = "GameData/GameDataBase.xml";
        //private const string FILE_EXTENSION = "content/GameDataBase.xml";
        
        /// <summary>
        /// Textures file path (Folder that contains the textures)
        /// </summary>
        private const string TEXTURE_DATA_FILE_EXTENSION = "GameData/TextureData/";
        //private const string TEXTURE_DATA_FILE_EXTENSION = "TextureData/";

        /// <summary>
        /// Load game data and store it locally
        /// </summary>
        /// <returns></returns>
        private GameData LoadData()
        {
           return ReadXML(GAME_DATA_FILE_EXTENSION);
        }

        /// <summary>
        /// Read Xml from the path
        /// </summary>
        /// <param name="a_strFilename"></param>
        /// <returns></returns>
        private GameData ReadXML(string a_strFilename)
        {
            GameData l_GameData = null;

            try
            {
                using (StreamReader reader = new StreamReader(a_strFilename))
                {
                    //XmlSerializer l_xmlSerializer = new XmlSerializer(typeof(GameInfo));
                    //l_xmlSerializer.Deserialize(reader.BaseStream);
                    //GameInfo.Instance = (GameInfo)l_xmlSerializer;

                    l_GameData = (GameData)new XmlSerializer(typeof(GameData)).Deserialize(reader.BaseStream);

                    Console.WriteLine("worked");

                }
            }
            catch (Exception a_ex)
            {
                l_GameData = new GameData();
                Console.WriteLine("ERROR: XML File Could not be deserialzed");
                Console.WriteLine("Exception Message:" + a_ex.Message);
            }

            return l_GameData;
        }

        /// <summary>
        /// Load texture data and store it locally
        /// </summary>
        /// <param name="a_graphicDevice"></param>
        public static void LoadTextureFromDisk(GraphicsDevice a_graphicDevice)
        {
            Instance.m_lstTextureData = new List<TextureData>(4); // SINCE I KNOW THERE ARE ONLY 4 CLUES IN TOTAL (OPTIMIZATION)
            for (int i = 0; i < LstClueData.Count; i++)
            {
                Clue l_Clue = LstClueData[i]; // caching
                string l_strId = l_Clue.TextureId;

                Texture2D l_texture = Texture2D.FromFile(a_graphicDevice, TEXTURE_DATA_FILE_EXTENSION + l_Clue.TextureId);
                Instance.m_lstTextureData.Add(new TextureData(l_strId, l_texture));
            }

            Instance.m_texNarrativeBg = Texture2D.FromFile(a_graphicDevice, TEXTURE_DATA_FILE_EXTENSION + GameData.Narrative.NarrativeBg);
        }

        /// <summary>
        /// Add all the locally store data extracted from files(aboves methods) into an orgainized list
        /// </summary>
        public static void AddCluesAndNarrativeDataInList()
        {
            Instance.m_lstClueData = new List<Clue>(4); // SINCE I KNOW THERE ARE ONLY 4 CLUES IN TOTAL (OPTIMIZATION)

            Instance.m_lstClueData.Add(GameData.PinClue.FirstClue);
            Instance.m_lstClueData.Add(GameData.PinClue.SecondClue);
            Instance.m_lstClueData.Add(GameData.PinClue.ThirdClue);

            Instance.m_lstClueData.Add(GameData.KeyClue.Clue);

            Instance.m_lstNarrativeData = new List<NarrativePage>();

            //Instance.m_lstNarrativeData.Add(GameData.IntroNarrative.Page1);
            //Instance.m_lstNarrativeData.Add(GameData.IntroNarrative.Page2);

            for (int i = 0; i < GameData.Narrative.Intro.Count; i++)
            {
                GameData.Narrative.Intro[i].Type = ENarrativeType.Intro; // Assign type
                Instance.m_lstNarrativeData.Add(GameData.Narrative.Intro[i]);
            }

            for (int i = 0; i < GameData.Narrative.PostSafeOpen.Count; i++)
            {
                GameData.Narrative.PostSafeOpen[i].Type = ENarrativeType.PostSafeOpen; // Assign type
                Instance.m_lstNarrativeData.Add(GameData.Narrative.PostSafeOpen[i]);
            }

            for (int i = 0; i < GameData.Narrative.End.Count; i++)
            {
                GameData.Narrative.End[i].Type = ENarrativeType.End; // Assign type
                Instance.m_lstNarrativeData.Add(GameData.Narrative.End[i]);
            }

            //Instance.m_lstNarrativeData.Add(GameData.IntroNarrative.Page1[0]);
            //Instance.m_lstNarrativeData.Add(GameData.IntroNarrative.Page2[0]);
        }

        /// <summary>
        /// Reset data for game restart
        /// </summary>
        public static void ResetData()
        {
            for (int i = 0; i < LstClueData.Count; i++)
            {
                LstClueData[i].IsDisplayClue = false;
            }

            for (int i = 0; i < LstNarrativeData.Count; i++)
            {
                LstNarrativeData[i].IsDisplay = false;
                LstNarrativeData[i].IsAlreadyDisplayed = false;
            }
        }
    }
}
