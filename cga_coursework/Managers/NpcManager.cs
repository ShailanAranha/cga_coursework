﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace cga_coursework
{
    class NpcManager
    {

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static NpcManager s_instance = null;
        private static NpcManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new NpcManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private NpcManager()
        {
        }

        /// <summary>
        /// Cat animtion object
        /// </summary>
        public Animation catAnimation;
        public static Animation CatAnimation { get { return Instance.catAnimation; } }

        /// <summary>
        /// Position of cat
        /// </summary>
        public Vector2 NpcPosition = Vector2.Zero;
        
        /// <summary>
        /// Position of player in previous iteration
        /// </summary>
        private Vector2 playerOldPosition = Vector2.Zero;

        /// <summary>
        /// Bounding rect of NPC
        /// </summary>
        private Rectangle NpcBoundingRectangle;

        /// <summary>
        /// Condition to transition to reaction state
        /// </summary>
        public bool IsPlayerTooCloseToNPC = false;

        /// <summary>
        /// Threshold distance between player and cat, if less then the above condition is true
        /// </summary>
        private const float THRESHOLD_DISTANCE = 200.0f;

        /// <summary>
        /// FSM Ai object
        /// </summary>
        private FsmAI m_fsmAI = null;

        /// <summary>
        /// Is npc collider visible?
        /// </summary>
        private bool IsColliderVisible = false;

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="a_catTexture"></param>
        public static void Initialize(Texture2D a_catTexture)
        {
            Instance.catAnimation = new Animation();

            //Texture2D l_catTexture = a_content.Load<Texture2D>(@"Graphics\NPC\cat_cat");

            float l_fltScreenWidth = ScreenDimensionManager.ScreenDimensions.X;
            float l_fltScreenHeight = ScreenDimensionManager.ScreenDimensions.Y;

            int frameCount = 4;

            int frameHeight = a_catTexture.Height;
            int frameWith = a_catTexture.Width / frameCount;
            float l_fltTextureScale = 0.2f;

            Instance.NpcPosition = new Vector2(l_fltScreenWidth / 1.5f, l_fltScreenHeight / 2);
            Instance.catAnimation.Initialize(a_catTexture, Instance.NpcPosition, frameWith, frameHeight, frameCount, 250.0f, Color.White, l_fltTextureScale, true);


            Instance.NpcBoundingRectangle = new Rectangle((int)Instance.NpcPosition.X -20, (int)Instance.NpcPosition.Y - 20, (int)(frameWith * l_fltTextureScale), (int)(frameHeight * l_fltTextureScale));

            Instance.InitializeFsmAI();
        }

        /// <summary>
        /// Initialize fsm AI
        /// </summary>
        private void InitializeFsmAI()
        {
            m_fsmAI = new FsmAI(this);
            //m_fsmAI = new FsmAI();
            // Instance.m_fsmAI = new FsmAI(Instance);

            // Create the states
            IdleState idle = new IdleState();
            ReactionState react = new ReactionState();
            // Create the transitions between the states
            idle.AddTransition(new Transition(react, () => Instance.IsPlayerTooCloseToNPC));
            react.AddTransition(new Transition(idle, () => !Instance.IsPlayerTooCloseToNPC));

            // Add the created states to the FSM
            m_fsmAI.AddState(idle);
            m_fsmAI.AddState(react);

            // Set the starting state of the FSM
            m_fsmAI.Initialise("Idle");
        }

        /// <summary>
        /// Activate cat aniamtion
        /// </summary>
        /// <param name="a_value"></param>
        public void ActivateCatAnimation(bool a_value)
        {
            catAnimation.m_isActive = a_value;
        }

        /// <summary>
        /// Pause cat animtion
        /// </summary>
        public void PauseAnimationLoop()
        {
            catAnimation.m_isLooping = false;
        }

        /// <summary>
        /// Resume cat animation
        /// </summary>
        public void ResumeAnimationLoop()
        {
            catAnimation.m_isLooping = true;
        }

        /// <summary>
        /// Play meow audio
        /// </summary>
        public void PlayMeow()
        {
            AudioManager.PlaySFX(GameAudio.CAT_MEOW);
        }

        /// <summary>
        /// Stop meow audio
        /// </summary>
        public void StopMeow()
        { 
            AudioManager.StopSFX(GameAudio.CAT_MEOW);
        }

        /// <summary>
        /// Loop meow audio
        /// </summary>
        /// <param name="a_isLoop"></param>
        public void LoopMeow(bool a_isLoop)
        { 
            AudioManager.LoopSFX(GameAudio.CAT_MEOW, a_isLoop);
        }

        /// <summary>
        /// If player collides with the cat
        /// </summary>
        public void CollsionDetection()
        {
            Rectangle l_playerBoundingRectangle = new Rectangle((int)(PlayerManager.Player.CurrentPosition.X - 20.0f), (int)(PlayerManager.Player.CurrentPosition.Y - 25.0f), PlayerManager.Player.Width * 2, PlayerManager.Player.Height * 2);

            if (NpcBoundingRectangle.Intersects(l_playerBoundingRectangle))
            {
                PlayerManager.Player.CurrentPosition = playerOldPosition;
            }

            playerOldPosition = PlayerManager.Player.CurrentPosition;
        }

        /// <summary>
        /// Game loop
        /// </summary>
        /// <param name="a_gameTime"></param>
        public static void Update(GameTime a_gameTime)
        { 
            Instance.catAnimation.Update(a_gameTime);
            
            //   if(Instance.NpcPosition - PlayerManager.Player.CurrentPosition >)

            if (Vector2.Distance(Instance.NpcPosition, PlayerManager.Player.CurrentPosition) < THRESHOLD_DISTANCE)
            {
                Instance.IsPlayerTooCloseToNPC = true;
            }
            else
            { 
                Instance.IsPlayerTooCloseToNPC = false;
            }

            Instance.m_fsmAI.Update(a_gameTime);
        }

        /// <summary>
        /// Draw animation
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void Draw(SpriteBatch a_spriteBatch)
        { 
            Instance.catAnimation.Draw(a_spriteBatch);
        }

        /// <summary>
        /// Draw collider on the screen (For texting)
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        /// <param name="a_graphicDevice"></param>
        public static void DrawCollider(SpriteBatch a_spriteBatch, GraphicsDevice a_graphicDevice)
        {
            if (Instance.IsColliderVisible)
            {
                Texture2D rect = new Texture2D(a_graphicDevice, Instance.NpcBoundingRectangle.Width, Instance.NpcBoundingRectangle.Height);

                Color[] data = new Color[Instance.NpcBoundingRectangle.Width * Instance.NpcBoundingRectangle.Height];
                for (int i = 0; i < data.Length; i++) data[i] = Color.Black;
                rect.SetData(data);
                a_spriteBatch.Draw(rect, Instance.NpcBoundingRectangle, Color.White);
            }
        }

        /// <summary>
        /// For testing (is collider visible?)
        /// </summary>
        public static void ToggleColliders()
        {
            Instance.IsColliderVisible = !Instance.IsColliderVisible;
        }
    }
}
