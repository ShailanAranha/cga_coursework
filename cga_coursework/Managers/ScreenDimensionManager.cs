﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class ScreenDimensionManager
    {

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static ScreenDimensionManager s_instance = null;
        private static ScreenDimensionManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new ScreenDimensionManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private ScreenDimensionManager()
        {

        }

        /// <summary>
        /// Stores screen height and width 
        /// </summary>
        private Vector2 m_screenDimensions = Vector2.Zero;
        public static Vector2 ScreenDimensions
        {
            get
            {
                return Instance.m_screenDimensions;
            }
        }

        /// <summary>
        /// Initialize and set scrren dimensions
        /// </summary>
        /// <param name="a_fltWidth"></param>
        /// <param name="a_fltHeight"></param>
        public static void Initialize(float a_fltWidth, float a_fltHeight)
        {
            Instance.m_screenDimensions = new Vector2(a_fltWidth, a_fltHeight);
        }
    }
}
