﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class AudioManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static AudioManager s_instance = null;
        private static AudioManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new AudioManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private AudioManager()
        {
        }

        /// <summary>
        /// Collection of background audios
        /// </summary>
        private Dictionary<string, Song> m_dicAudios = null;

        /// <summary>
        /// Collection of Sfx
        /// </summary>
        private Dictionary<string, SFX> m_dicSFXs = null;

        /// <summary>
        /// Register background audio to the collection
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_audio"></param>
        public static void RegisterAudio(string a_strId, Song a_audio)
        {
            if (Instance.m_dicAudios.ContainsKey(a_strId))
            {
                return;
            }

            Instance.m_dicAudios.Add(a_strId, a_audio);
        }

        /// <summary>
        /// Register SFX to the collection
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_sfx"></param>
        public static void RegisterSFX(string a_strId, SFX a_sfx)
        {
            if (Instance.m_dicSFXs.ContainsKey(a_strId))
            {
                return;
            }

            Instance.m_dicSFXs.Add(a_strId, a_sfx);
        }

        /// <summary>
        /// Initiaze audio collections
        /// </summary>
        public static void Initialize()
        {
            if (Instance.m_dicAudios != null)
            {
                return;
            }

            Instance.m_dicAudios = new Dictionary<string, Song>();
            Instance.m_dicSFXs = new Dictionary<string, SFX>();
        }

        /// <summary>
        /// PLay background audio using ID
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_callBack"></param>
        public static void PlayAudio(string a_strId, Action a_callBack = null)
        {

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(Instance.m_dicAudios[a_strId]);

            a_callBack?.Invoke();
        }

        /// <summary>
        /// PLay SFX using ID
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_callBack"></param>
        public static void PlaySFX(string a_strId, Action a_callBack = null)
        {
            Instance.m_dicSFXs[a_strId].SoundFXInstance.Play();

            a_callBack?.Invoke();
        }

        /// <summary>
        /// Stop background audio
        /// </summary>
        public static void StopAudio()
        {
            MediaPlayer.Stop();
        }

        /// <summary>
        /// Stop SFX
        /// </summary>
        /// <param name="a_strId"></param>
        public static void StopSFX(string a_strId)
        {
            Instance.m_dicSFXs[a_strId].SoundFXInstance.Stop();
        }

        /// <summary>
        /// Loop sfx audio
        /// </summary>
        /// <param name="a_strId"></param>
        /// <param name="a_isLoop"></param>
        public static void LoopSFX(string a_strId, bool a_isLoop)
        {
            Instance.m_dicSFXs[a_strId].SoundFXInstance.IsLooped = a_isLoop;
        }

        /// <summary>
        /// Unload all background audio files
        /// </summary>
        public static void UnloadAllAudio()
        {
            foreach (KeyValuePair<string, Song> l_audio in Instance.m_dicAudios)
            {
                l_audio.Value.Dispose();
            }
        }

        /// <summary>
        /// Unload all SFX audio files
        /// </summary>
        public static void UnloadAllSfx()
        {
            foreach (KeyValuePair<string, SFX> l_sfx in Instance.m_dicSFXs)
            {
                l_sfx.Value.SoundFXInstance.Dispose();
            }
        }
    }

    /// <summary>
    /// Store and handles SFX 
    /// </summary>
    class SFX
    {
        /// <summary>
        /// SFX object
        /// </summary>
        public readonly SoundEffect SoundFX;

        /// <summary>
        /// SFX instance
        /// </summary>
        public readonly SoundEffectInstance SoundFXInstance;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="a_soundFX"></param>
        public SFX(SoundEffect a_soundFX)
        {
            SoundFX = a_soundFX;
            SoundFXInstance = SoundFX.CreateInstance();
        }

    }
}
