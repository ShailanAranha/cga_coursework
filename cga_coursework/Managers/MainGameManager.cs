﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace cga_coursework
{
    class MainGameManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static MainGameManager s_instance = null;
        private static MainGameManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new MainGameManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private MainGameManager()
        {
        }

        /// <summary>
        /// Is Main game active?
        /// </summary>
        private bool m_isMainGameActive = false;

        /// <summary>
        /// Timer to delay the narrative display
        /// </summary>
        private double m_dNarrativeTimer = 0.0f;

        /// <summary>
        /// Is Intro narrative shown?
        /// </summary>
        private bool m_isNarrativeShown = false;

        /// <summary>
        /// Game timer display
        /// </summary>
        private DisplayTextData m_displayBossTimer = null;
        
        /// <summary>
        /// Game timer
        /// </summary>
        private double m_dBossTimer = 0.0f;

        /// <summary>
        /// Is game timer active?
        /// </summary>
        private bool m_isBossTimerActive = false;

        /// <summary>
        /// Game timer in reverse (Reverse countdown to display)
        /// </summary>
        private double m_fltReverseTimer = 0.0f;

        /// <summary>
        /// Title of game timer
        /// </summary>
        private string m_strTimerHeader = null;

        /// <summary>
        /// Load data in the main game
        /// </summary>
        /// <param name="a_content"></param>
        /// <param name="a_graphicsDevice"></param>
        public static void LoadData(ContentManager a_content, GraphicsDevice a_graphicsDevice)
        {
            // ************ PLAYER ANIMTION ***************

            Animation l_animPlayerRightWalk = new Animation();
            Texture2D l_texPlayerRightWalk = a_content.Load<Texture2D>(@"Graphics\Player\WalkingRight");
            l_animPlayerRightWalk.Initialize(l_texPlayerRightWalk, Vector2.Zero, Color.White, 2.0f, true);

            Animation l_animPlayerLeftWalk = new Animation();
            Texture2D l_texPlayerLeftWalk = a_content.Load<Texture2D>(@"Graphics\Player\WalkingLeft");
            l_animPlayerLeftWalk.Initialize(l_texPlayerLeftWalk, Vector2.Zero, Color.White, 2.0f, true);

            Animation l_animPlayerUpWalk = new Animation();
            Texture2D l_texPlayerUpWalk = a_content.Load<Texture2D>(@"Graphics\Player\WalkingUp");
            l_animPlayerUpWalk.Initialize(l_texPlayerUpWalk, Vector2.Zero, Color.White, 2.0f, true);

            Animation l_animPlayerDownWalk = new Animation();
            Texture2D l_texPlayerDownWalk = a_content.Load<Texture2D>(@"Graphics\Player\WalkingDown");
            l_animPlayerDownWalk.Initialize(l_texPlayerDownWalk, Vector2.Zero, Color.White, 2.0f, true);

            //Vector2 l_playerPosition = new Vector2(a_graphicsDevice.Viewport.TitleSafeArea.X, a_graphicsDevice.Viewport.TitleSafeArea.Y + a_graphicsDevice.Viewport.TitleSafeArea.Height / 2);
            Vector2 l_playerPosition = new Vector2(517, 658);
            PlayerManager.LoadData(a_content.Load<Texture2D>(@"Graphics\Player\Idle"), l_animPlayerRightWalk, l_animPlayerLeftWalk, l_animPlayerUpWalk, l_animPlayerDownWalk, l_playerPosition);

            // ************ NPC ANIMATION ***************
            NpcManager.Initialize(a_content.Load<Texture2D>(@"Graphics\NPC\cat_cat"));

            // ************ HEADS UP DISPLAY ***************

            HudManager.LoadFont(a_content.Load<SpriteFont>(@"Graphics\Font\gameFont"));


            // ************ GAME DATA (DATA-DRIVEN) ***************

            GameData l_temp = GameDataManager.GameData; // TO MAKE SURE THAT DATA IS LOADED
            GameDataManager.LoadTextureFromDisk(a_graphicsDevice);


            // ************ ENVIRONMENT OBJECTS (INTERACTIVE AND NON-INTERACTIVE) ***************
            IReadOnlyList<GameObject> l_lstInteractbaleObjects = EnvironmentManager.InteractableObjects.Objects;  // Caching

            for (int i = 0; i < l_lstInteractbaleObjects.Count; i++)
            {
                l_lstInteractbaleObjects[i].Initialize(a_content.Load<Texture2D>(string.Format(@"Graphics\Room\{0}", l_lstInteractbaleObjects[i].Id)));
            }

            IReadOnlyList<GameObject> l_lstNonInteractbaleObjects = EnvironmentManager.NonInteractbaleObjects.Objects;  // Caching

            for (int i = 0; i < l_lstNonInteractbaleObjects.Count; i++)
            {
                l_lstNonInteractbaleObjects[i].Initialize(a_content.Load<Texture2D>(string.Format(@"Graphics\Room\{0}", l_lstNonInteractbaleObjects[i].Id)));
            }

            // ************ INITIALIZE OTHER MANAGERS ***************

            GuidanceManager.Initialize(a_content.Load<Texture2D>(@"Graphics\Guidance\Controller_A"), a_content.Load<Texture2D>(@"Graphics\Guidance\Controller_X"), a_content.Load<Texture2D>(@"Graphics\Guidance\i_btn"), a_graphicsDevice.Viewport.TitleSafeArea.Height, a_graphicsDevice.Viewport.TitleSafeArea.Width);
            SafeManager.Initialize(a_content.Load<Texture2D>(@"Graphics\Safe\Safe"), a_content.Load<Texture2D>(@"Graphics\Bg\transparent"), a_graphicsDevice.Viewport.TitleSafeArea.Width, a_graphicsDevice.Viewport.TitleSafeArea.Height);
            OverlayManager.Initiatize(a_content.Load<Texture2D>(@"Graphics\Bg\transparent"));
            KeyManager.Initialize();

            // ************ AUDIO ***************

            // BACKGROUND
            AudioManager.Initialize();
            AudioManager.RegisterAudio(GameAudio.MAIN_GAME, a_content.Load<Song>(@"Audio\" + GameAudio.MAIN_GAME));

            // SFX

            AudioManager.RegisterSFX(GameAudio.CORRECT_PIN, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.CORRECT_PIN)));
            AudioManager.RegisterSFX(GameAudio.INCORRECT_PIN, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.INCORRECT_PIN)));
            AudioManager.RegisterSFX(GameAudio.TYPING_PIN, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.TYPING_PIN)));
            AudioManager.RegisterSFX(GameAudio.OPEN_CLUE, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.OPEN_CLUE)));
            AudioManager.RegisterSFX(GameAudio.PAGE_TURN, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.PAGE_TURN)));
            AudioManager.RegisterSFX(GameAudio.CAT_MEOW, new SFX(a_content.Load<SoundEffect>(@"Audio\" + GameAudio.CAT_MEOW)));

        }

        /// <summary>
        /// Initialize main game elements
        /// </summary>
        public static void Initialize()
        {
            // ************ PLAYER ***************

            PlayerManager.Initialize();

            // ************ REGISTER EVENTS ***************
            EnvironmentManager.RegisterEvent(OverlayManager.DisplayClue);
            EnvironmentManager.RegisterEvent(KeyManager.EnableKey);
            EnvironmentManager.RegisterEvent(SafeManager.DisplaySafeUI);
            SafeManager.OnSuccessfullPinEntry += () => { EnvironmentManager.UnregisterEvent(SafeManager.DisplaySafeUI); };
           // SafeManager.OnSuccessfullPinEntry += ShowPostSafeOpenNarrative;
            //InputManager.OnPressedContinueBtn += OverlayManager.HideCurrentInstruction;
            InputManager.OnEnterPinNum += SafeManager.UserInput;
            InputManager.OnEnterPinNum += KeyManager.UserInput;
            InputManager.OnPressedTestingKey += EnvironmentManager.ToggleEnvironmentColliders;
            InputManager.OnPressedTestingKey += PlayerManager.ToggleColliders;
            InputManager.OnPressedTestingKey += NpcManager.ToggleColliders;
            KeyManager.OnSuccessfullKeyEntry += ShowEndNarrative;
            SavingManager.OnDataFlushed -= ResetGameTimer;
            SavingManager.OnDataFlushed += ResetGameTimer;


            // ************ GAME TIMER ***************
            float l_fltScreenWidth = ScreenDimensionManager.ScreenDimensions.X;
            float l_fltScreenHeight = ScreenDimensionManager.ScreenDimensions.Y;
            Instance.m_displayBossTimer = new DisplayTextData("", "", new Vector2(l_fltScreenWidth-300, 80), Color.White);
            HudManager.RegisterTextOnHud(Instance.m_displayBossTimer);
        }

        /// <summary>
        /// Unload main game contents 
        /// </summary>
        public static void UnloadContent()
        {
            // UNLOAD GRAPHICS


            IReadOnlyList<GameObject> l_lstInteractbaleObjects = EnvironmentManager.InteractableObjects.Objects;  // Caching

            for (int i = 0; i < l_lstInteractbaleObjects.Count; i++)
            {
                l_lstInteractbaleObjects[i].Texture.Dispose();
            }

            IReadOnlyList<GameObject> l_lstNonInteractbaleObjects = EnvironmentManager.NonInteractbaleObjects.Objects;  // Caching

            for (int i = 0; i < l_lstNonInteractbaleObjects.Count; i++)
            {
                l_lstNonInteractbaleObjects[i].Texture.Dispose();
            }

            IReadOnlyList<TextureData> l_lstTextureData = GameDataManager.LstTextureData; // Caching

            for (int i = 0; i < l_lstTextureData.Count; i++)
            {
                l_lstTextureData[i].TexImg.Dispose();
            }

            PlayerManager.Player.PlayerRightWalkAnim.TexSpriteStrip.Dispose();
            PlayerManager.Player.PlayerLeftWalkAnim.TexSpriteStrip.Dispose();
            PlayerManager.Player.PlayerDownWalkAnim.TexSpriteStrip.Dispose();
            PlayerManager.Player.PlayerUpWalkAnim.TexSpriteStrip.Dispose();

            NpcManager.CatAnimation.TexSpriteStrip.Dispose();
            HudManager.Font.Texture.Dispose();
            SafeManager.BgTexture.Dispose();
            SafeManager.SafeTexture.Dispose();

            // UNLOAD AUDIO
            AudioManager.UnloadAllAudio();
            AudioManager.UnloadAllSfx();
        }

        /// <summary>
        /// Show main game
        /// </summary>
        public static void ShowMainGame()
        {
            Instance.m_isMainGameActive = true;

            //AudioManager.PlayAudio(GameAudio.MAIN_GAME);

            Instance.m_fltReverseTimer = SavingManager.SavedData.GameTime;
            Instance.m_strTimerHeader = GameDataManager.GameData.EnemyHeader;

            bool m_isNarativeCompleted = SavingManager.SavedData.IsIntroNarrativeCompleted;

            Instance.m_isNarrativeShown = m_isNarativeCompleted;

            if (Instance.m_isNarrativeShown)
            {
                StartBossTimer();
            }
        }

        /// <summary>
        /// Hide main game
        /// </summary>
        public static void HideMainGame()
        {
            Instance.m_isMainGameActive = false;

            // RESET DATA
            Instance.m_isNarrativeShown = false;
            Instance.m_dNarrativeTimer = 0.0f;
           
            GameDataManager.ResetData();
            ResetGameTimer();

            AudioManager.StopAudio();
        }

        /// <summary>
        /// Reset game timer for new game/restart
        /// </summary>
        private static void ResetGameTimer()
        {
            double l_dGameTime = 0.0f;

            if (!double.TryParse(GameDataManager.GameData.GameTime, out l_dGameTime))
            {
                ;  // TRY PARSE FAILED
            }

            Instance.m_fltReverseTimer = l_dGameTime * 60.0f;
            Instance.m_isNarrativeShown = false;
            StopBossTimer();
            Instance.m_isBossTimerActive = false;
            Instance.m_dBossTimer = 0.0f;
            Instance.m_displayBossTimer.DisplayText = string.Empty;
            SavingManager.SaveGameTime(Instance.m_fltReverseTimer);
        }

        /// <summary>
        /// Narrative when game begins
        /// </summary>
        private static void ShowIntroNarrative()
        {
           OverlayManager.DisplayNarrative(ENarrativeType.Intro, StartBossTimer);
        }

        /// <summary>
        /// Narrative when player manages to figure the PIN and to open the safe
        /// </summary>
        public static void ShowPostSafeOpenNarrative()
        {
            OverlayManager.DisplayNarrative(ENarrativeType.PostSafeOpen);
        }

        /// <summary>
        /// Narrative when player manages to figure the KEY and to open the door
        /// </summary>
        private static void ShowEndNarrative()
        {
            OverlayManager.DisplayNarrative(ENarrativeType.End, ShowEndScreen);
        }

        /// <summary>
        /// Start Game timer
        /// </summary>
        private static void StartBossTimer()
        {
            Instance.m_isBossTimerActive = true;
            SavingManager.SaveIntroNarrativeData(true);

            AudioManager.PlayAudio(GameAudio.MAIN_GAME);
        }

        /// <summary>
        /// Stop Game timer
        /// </summary>
        private static void StopBossTimer()
        {
            Instance.m_isBossTimerActive = false;
        }

        /// <summary>
        /// Show end screen UI
        /// </summary>
        private static void ShowEndScreen()
        {
            //FSMGameManager.ExitGameState(EGameState.MainGame);
            //FSMGameManager.EnterGameState(EGameState.EndScreen);

            Task l_task = Instance.DelayEndScreen();
            TaskAwaiter l_awaiter = l_task.GetAwaiter();
            l_awaiter.OnCompleted(l_task.Dispose);
        }

        /// <summary>
        /// Delay executor (Multithreading)
        /// </summary>
        /// <returns></returns>
        private async Task DelayEndScreen()
        {
            await Task.Delay(TimeSpan.FromSeconds(1.0f));
            FSMGameManager.ExitGameState(EGameState.MainGame);
            FSMGameManager.EnterGameState(EGameState.EndScreen);
        }

        /// <summary>
        /// Game loop to update main game elments
        /// </summary>
        /// <param name="a_gametime"></param>
        /// <param name="a_graphicDevice"></param>
        public static void Update(GameTime a_gametime, GraphicsDevice a_graphicDevice)
        {
            if (!Instance.m_isMainGameActive)
            {
                return;
            }

            PlayerManager.UpdatePlayer(a_gametime, a_graphicDevice);
            NpcManager.Update(a_gametime);
            EnvironmentManager.NonInteractbaleObjects.Update(a_gametime, PlayerManager.Player);
            EnvironmentManager.InteractableObjects.Update(a_gametime, PlayerManager.Player);

            Instance.m_dNarrativeTimer += a_gametime.ElapsedGameTime.TotalMilliseconds;

            if (!Instance.m_isNarrativeShown && Instance.m_dNarrativeTimer > 1000.0f)
            {
                ShowIntroNarrative();
                Instance.m_dNarrativeTimer = 0.0f;
                Instance.m_isNarrativeShown = true;
                SavingManager.SaveIntroNarratibeData(Instance.m_isNarrativeShown);
            }

            if (Instance.m_isBossTimerActive)
            {

                Instance.m_dBossTimer += a_gametime.ElapsedGameTime.TotalSeconds;


                double displayTimeInSeconds = Instance.m_fltReverseTimer - Instance.m_dBossTimer;

                if (displayTimeInSeconds < 0.0)
                {
                    StopBossTimer();
                    ShowEndScreen();
                    // stop
                }

                TimeSpan t = TimeSpan.FromSeconds(displayTimeInSeconds);

                string displayTimeInMinutes = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);

                
                int count = Instance.m_strTimerHeader.Length;
                count /= 2;
                string l_strTotalSpace = string.Empty;
                for (int i = 0; i < count; i++) 
                {
                    displayTimeInMinutes = " " + displayTimeInMinutes; // ALIGNMENT OF THE CLOCK
                }
                   // displayTimeInMinutes = displayTimeInMinutes.Replace(displayTimeInMinutes, l_strTotalSpace + displayTimeInMinutes);

                Instance.m_displayBossTimer.DisplayText = Instance.m_strTimerHeader +'\n'+ displayTimeInMinutes;

                SavingManager.SaveGameTime(displayTimeInSeconds);
            }
        }

        /// <summary>
        /// Draw main game elements on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        /// <param name="a_graphicsDevice"></param>
        public static void Draw(SpriteBatch a_spriteBatch, GraphicsDevice a_graphicsDevice)
        {
            if (!Instance.m_isMainGameActive)
            {
                return;
            }

            IReadOnlyList<GameObject> l_lstNonInteractbaleObjects = EnvironmentManager.NonInteractbaleObjects.Objects;  // Caching

            for (int i = 0; i < l_lstNonInteractbaleObjects.Count; i++)
            {
                l_lstNonInteractbaleObjects[i].Draw(a_spriteBatch);
                l_lstNonInteractbaleObjects[i].DrawCollider(a_spriteBatch, a_graphicsDevice);
            }

            IReadOnlyList<GameObject> l_lstInteractbaleObjects = EnvironmentManager.InteractableObjects.Objects;  // Caching

            for (int i = 0; i < l_lstInteractbaleObjects.Count; i++)
            {
                l_lstInteractbaleObjects[i].Draw(a_spriteBatch);
                l_lstInteractbaleObjects[i].DrawCollider(a_spriteBatch, a_graphicsDevice);
            }
            //m_room.Draw(a_spriteBatch);

            PlayerManager.DrawCollider(a_spriteBatch, a_graphicsDevice);
            PlayerManager.Draw(a_spriteBatch);

            NpcManager.Draw(a_spriteBatch);
            NpcManager.DrawCollider(a_spriteBatch, a_graphicsDevice);

            OverlayManager.DrawClue(a_spriteBatch);
            OverlayManager.DrawNarrative(a_spriteBatch);

            GuidanceManager.DrawGuidance(a_spriteBatch);
            SafeManager.Draw(a_spriteBatch);
            HudManager.DrawText(a_spriteBatch);
        }
    }
}
