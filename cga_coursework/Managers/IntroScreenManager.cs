﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;

namespace cga_coursework
{
    class IntroScreenManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static IntroScreenManager s_instance = null;
        private static IntroScreenManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new IntroScreenManager();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private IntroScreenManager()
        {
        }

        /// <summary>
        /// Is intro screen visible?
        /// </summary>
        private bool m_isVisbile = false;

        /// <summary>
        /// Current active screen, PlayScreen/ControlsScreen
        /// </summary>
        private EStartScreen m_ECurrentActiveScreen = EStartScreen.PlayScreen;
        
        // PLAY SCREEN

        /// <summary>
        /// Play screen background 
        /// </summary>
        private GameObject m_objBg = null;

        /// <summary>
        /// Intro screen buttons
        /// </summary>
        private Button m_btnContinue = null;
        private Button m_btnPlay = null;
        private Button m_btnControls = null;
        private Button m_btnExit = null;

        /// <summary>
        /// Is contnue button visible?
        /// </summary>
        private bool m_isContinueBtnVisbile = false;

        /// <summary>
        /// Current button selected
        /// </summary>
        private EStartScreenOptions m_ECurrentSelectedBtn = EStartScreenOptions.Play;

        /// <summary>
        /// Event invoked when quit button is clicked
        /// </summary>
        public static event Action onClickQuit = null;

        /// <summary>
        /// Intro content ids
        /// </summary>
        private const string BG_TEXTURE_ID = "Start_screen_bg";
        private const string CONTINUE_BTN_ID = "continue_btn";
        private const string PLAY_BTN_ID = "play_btn";
        private const string CONTROLS_ID = "controls_btn";
        private const string EXIT_ID = "exit_btn";

        //CONTROLS SCREEN

        /// <summary>
        /// controls screen background 
        /// </summary>
        private GameObject m_objControlsBg = null;

        /// <summary>
        /// Back button object
        /// </summary>
        private Button m_btnBack = null;

        /// <summary>
        /// Controls content id
        /// </summary>
        private const string CONTROLS_TEXTURE_ID = "controls";
        private const string BACK_BTN_ID = "back_btn";

        /// <summary>
        /// Load data in the intro screen UI
        /// </summary>
        /// <param name="a_content"></param>
        public static void LoadData(ContentManager a_content)
        {
           InitializePlayScreen(a_content.Load<Texture2D>(@"Graphics\StartScreen\start_bg"),
                a_content.Load<Texture2D>(@"Graphics\StartScreen\continue_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\continue_highlighted"),
                a_content.Load<Texture2D>(@"Graphics\StartScreen\play_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\play_highlighted"),
                a_content.Load<Texture2D>(@"Graphics\StartScreen\controls_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\controls_highlighted"),
                a_content.Load<Texture2D>(@"Graphics\StartScreen\quit_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\quit_highlighted"));


            InitializeControlsScreen(a_content.Load<Texture2D>(@"Graphics\StartScreen\controls"),
              a_content.Load<Texture2D>(@"Graphics\StartScreen\back_normal"), a_content.Load<Texture2D>(@"Graphics\StartScreen\back_highlighted"));


            // AUDIO

            AudioManager.Initialize();
            
            AudioManager.RegisterAudio(GameAudio.INTRO_GAME, a_content.Load<Song>(@"Audio\"+ GameAudio.INTRO_GAME));

            // SFX
            AudioManager.RegisterSFX(GameAudio.BUTTON_TRANSITION, new SFX(a_content.Load<SoundEffect>(@"Audio\"+ GameAudio.BUTTON_TRANSITION)) );
            AudioManager.RegisterSFX(GameAudio.BUTTON_CLICK, new SFX(a_content.Load<SoundEffect>(@"Audio\"+ GameAudio.BUTTON_CLICK)) );
            
        }

        /// <summary>
        /// Initialize intro Screen properites (Not used at the moment)
        /// </summary>
        /// <param name="a_texBg"></param>
        /// <param name="a_btnContinue"></param>
        /// <param name="a_btnPlay"></param>
        /// <param name="a_btnControls"></param>
        /// <param name="a_btnExit"></param>
        private static void Initialize(Texture2D a_texBg, Button a_btnContinue, Button a_btnPlay, Button a_btnControls, Button a_btnExit)
        {
            Instance.m_objBg = new GameObject(BG_TEXTURE_ID, Vector2.Zero, false);

            Instance.m_objBg.Initialize(a_texBg);
            Instance.m_btnContinue = a_btnContinue;
            Instance.m_btnPlay = a_btnPlay;
            Instance.m_btnControls = a_btnControls;
            Instance.m_btnExit = a_btnExit;

        }

        /// <summary>
        /// Initialize Play Screen properites
        /// </summary>
        /// <param name="a_texBg"></param>
        /// <param name="a_texContinueNormal"></param>
        /// <param name="a_texContinueHighlighted"></param>
        /// <param name="a_texPlayNormal"></param>
        /// <param name="a_texPlayHighlighted"></param>
        /// <param name="a_texControlsNormal"></param>
        /// <param name="a_texControlsHighlighted"></param>
        /// <param name="a_texExitNormal"></param>
        /// <param name="a_texExitHighlighted"></param>
        private static void InitializePlayScreen(Texture2D a_texBg, Texture2D a_texContinueNormal, Texture2D a_texContinueHighlighted,
             Texture2D a_texPlayNormal, Texture2D a_texPlayHighlighted, Texture2D a_texControlsNormal, Texture2D a_texControlsHighlighted, 
             Texture2D a_texExitNormal, Texture2D a_texExitHighlighted)
        {
            Instance.m_objBg = new GameObject(BG_TEXTURE_ID, Vector2.Zero, false);
            Instance.m_objBg.Initialize(a_texBg);

            Vector2 l_vec2ContinuePos = Vector2.Zero;
            Vector2 l_vec2PlayPos = Vector2.Zero;
            Vector2 l_vec2ControlPos = Vector2.Zero;
            Vector2 l_vec2ExitPos = Vector2.Zero;

            float l_fltHalfWidth = ScreenDimensionManager.ScreenDimensions.X/2 - 200;
            float l_fltHalfHeight = ScreenDimensionManager.ScreenDimensions.Y/2;

            Instance.m_isContinueBtnVisbile = SavingManager.OldSessionExists && !SavingManager.SavedData.IsOldSessionCompleted;
           
            if (Instance.m_isContinueBtnVisbile)
            //if (SavingManager.OldSessionExists)
                {
                l_vec2ContinuePos = new Vector2(l_fltHalfWidth, l_fltHalfHeight - 100);
                l_vec2PlayPos = new Vector2(l_fltHalfWidth , l_fltHalfHeight );
                l_vec2ControlPos = new Vector2(l_fltHalfWidth , l_fltHalfHeight + 100);
                l_vec2ExitPos = new Vector2(l_fltHalfWidth , l_fltHalfHeight + 200);

                Instance.m_ECurrentSelectedBtn = EStartScreenOptions.Continue;
            }
            else
            {
                l_vec2PlayPos = new Vector2(l_fltHalfWidth, l_fltHalfHeight - 100);
                l_vec2ControlPos = new Vector2(l_fltHalfWidth, l_fltHalfHeight );
                l_vec2ExitPos = new Vector2(l_fltHalfWidth, l_fltHalfHeight + 100);
                Instance.m_ECurrentSelectedBtn = EStartScreenOptions.Play;
            }

            Instance.m_btnContinue = new Button(CONTINUE_BTN_ID, l_vec2ContinuePos,new Vector2(0.5f, 0.5f), a_texContinueNormal, a_texContinueHighlighted);
            Instance.m_btnPlay = new Button(PLAY_BTN_ID, l_vec2PlayPos, new Vector2(0.5f, 0.5f), a_texPlayNormal, a_texPlayHighlighted);
            Instance.m_btnControls = new Button(CONTROLS_ID, l_vec2ControlPos, new Vector2(0.5f, 0.5f), a_texControlsNormal, a_texControlsHighlighted);
            Instance.m_btnExit = new Button(EXIT_ID, l_vec2ExitPos, new Vector2(0.5f, 0.5f), a_texExitNormal, a_texExitHighlighted);

            Instance.m_btnContinue.OnClick += Instance.OnClickContinueBtn;
            Instance.m_btnPlay.OnClick += Instance.OnClickPlayBtn;
            Instance.m_btnControls.OnClick += Instance.OnClickControlBtn;
            Instance.m_btnExit.OnClick += Instance.OnClickExitBtn;
        }

        /// <summary>
        /// Initialize Controls Screen properites
        /// </summary>
        /// <param name="a_texBg"></param>
        /// <param name="a_texBackNormal"></param>
        /// <param name="a_texBackHighlighted"></param>
        private static void InitializeControlsScreen(Texture2D a_texBg, Texture2D a_texBackNormal, Texture2D a_texBackHighlighted)
        {
            Instance.m_objControlsBg = new GameObject(CONTROLS_TEXTURE_ID, Vector2.Zero, false);
            Instance.m_objControlsBg.Initialize(a_texBg);

   
            Vector2 l_vec2BackPos = Vector2.Zero;

            float l_fltHalfWidth = ScreenDimensionManager.ScreenDimensions.X / 2 - 200;
            float l_fltHalfHeight = ScreenDimensionManager.ScreenDimensions.Y / 2;


            l_vec2BackPos = new Vector2(l_fltHalfWidth, l_fltHalfHeight + 250);
   
            Instance.m_btnBack = new Button(BACK_BTN_ID, l_vec2BackPos, new Vector2(0.5f, 0.5f), a_texBackNormal, a_texBackHighlighted);

            Instance.m_btnBack.OnClick += Instance.OnClickBackBtn;
        }

        /// <summary>
        /// Unload intro game contents 
        /// </summary>
        public static void UnloadContent()
        {
            Instance.m_objBg.Texture.Dispose();
            Instance.m_objControlsBg.Texture.Dispose();

            Instance.m_btnBack.Texture.Dispose();
            Instance.m_btnContinue.Texture.Dispose();
            Instance.m_btnPlay.Texture.Dispose();
            Instance.m_btnControls.Texture.Dispose();
            //Instance.m_btnExit.Texture.Dispose();
        }

        /// <summary>
        /// Show Intro Game 
        /// </summary>
        public static void ShowUI()
        {
            InputManager.OnPressedUp += Instance.SwitchBtnUp;
            InputManager.OnPressedDown += Instance.SwitchBtnDown;
            Instance.m_isVisbile = true;
            
            AudioManager.PlayAudio(GameAudio.INTRO_GAME);
        }

        /// <summary>
        /// Hide intro game
        /// </summary>
        public static void HideUI()
        {
            Instance.m_isVisbile = false;
            InputManager.OnPressedUp -= Instance.SwitchBtnUp;
            InputManager.OnPressedDown -= Instance.SwitchBtnDown;

            Instance.m_btnContinue.OnClick -= Instance.OnClickContinueBtn;
            Instance.m_btnPlay.OnClick -= Instance.OnClickPlayBtn;
            Instance.m_btnControls.OnClick -= Instance.OnClickControlBtn;
            Instance.m_btnExit.OnClick -= Instance.OnClickExitBtn;

            Instance.m_btnBack.OnClick -= Instance.OnClickBackBtn;

            AudioManager.StopAudio();
            UnloadContent();
        }

        /// <summary>
        /// Navigate down for play screen buttons
        /// </summary>
        private void SwitchBtnDown()
        {
            if (m_ECurrentActiveScreen.Equals(EStartScreen.ControlsScreen))
            {
                return;
            }

            int l_iCurrentActiveBtn = (int)Instance.m_ECurrentSelectedBtn;

            if(Instance.m_isContinueBtnVisbile)
            //if (SavingManager.OldSessionExists)
            {
                l_iCurrentActiveBtn = ++l_iCurrentActiveBtn % 4;
            }
            else
            {
                //l_iCurrentActiveBtn = ++l_iCurrentActiveBtn % 3;
                //l_iCurrentActiveBtn = ++l_iCurrentActiveBtn;

                if (l_iCurrentActiveBtn == 3)
                {
                    l_iCurrentActiveBtn = 1;
                }
                else
                {
                    l_iCurrentActiveBtn = ++l_iCurrentActiveBtn;
                }
            }

            Instance.m_ECurrentSelectedBtn = (EStartScreenOptions)l_iCurrentActiveBtn;

            AudioManager.StopSFX(GameAudio.BUTTON_TRANSITION);
            AudioManager.PlaySFX(GameAudio.BUTTON_TRANSITION);
        }

        /// <summary>
        /// Navigate up for play screen buttons
        /// </summary>
        private void SwitchBtnUp()
        {
            if (m_ECurrentActiveScreen.Equals(EStartScreen.ControlsScreen))
            {
                return;
            }

            int l_iCurrentActiveBtn = (int)Instance.m_ECurrentSelectedBtn;

            if(Instance.m_isContinueBtnVisbile)
            //if (SavingManager.OldSessionExists)
            {

                if (l_iCurrentActiveBtn == 0)
                {
                    l_iCurrentActiveBtn = 3;
                }
                else
                { 
                    l_iCurrentActiveBtn = --l_iCurrentActiveBtn ;
                }
            }
            else
            {
                if (l_iCurrentActiveBtn == 1)
                {
                    l_iCurrentActiveBtn = 3;
                }
                else
                {
                    l_iCurrentActiveBtn = --l_iCurrentActiveBtn;
                }
            }

            Instance.m_ECurrentSelectedBtn = (EStartScreenOptions)l_iCurrentActiveBtn;

            AudioManager.StopSFX(GameAudio.BUTTON_TRANSITION);
            AudioManager.PlaySFX(GameAudio.BUTTON_TRANSITION);
        }

        /// <summary>
        /// On click continue button
        /// </summary>
        public void OnClickContinueBtn()
        {
            FSMGameManager.ExitGameState(EGameState.IntroScreen);
            FSMGameManager.EnterGameState(EGameState.MainGame);

            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);
        }

        /// <summary>
        /// On click play button
        /// </summary>
        public void OnClickPlayBtn()
        {
            SavingManager.FlushData();
            SavingManager.SaveOldSessionData(false);
            FSMGameManager.ExitGameState(EGameState.IntroScreen);
            FSMGameManager.EnterGameState(EGameState.MainGame);

            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);
        }

        /// <summary>
        /// On Click controls button
        /// </summary>
        public void OnClickControlBtn()
        {
            m_ECurrentActiveScreen = EStartScreen.ControlsScreen;

            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);
        }

        /// <summary>
        /// On Click Exit button 
        /// </summary>
        public void OnClickExitBtn()
        {
            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);

            onClickQuit?.Invoke();
        }

        /// <summary>
        /// On Click back button
        /// </summary>
        public void OnClickBackBtn()
        {
            m_ECurrentActiveScreen = EStartScreen.PlayScreen;
            m_btnControls.ResetEvent();

            AudioManager.StopSFX(GameAudio.BUTTON_CLICK);
            AudioManager.PlaySFX(GameAudio.BUTTON_CLICK);
        }

        /// <summary>
        /// Game loop to update intro game elments
        /// </summary>
        /// <param name="a_gameTime"></param>
        public static void Update(GameTime a_gameTime)
        {
            if (!Instance.m_isVisbile)
            {
                return;
            }

            Instance.m_objBg.Update(a_gameTime);
            Instance.m_btnContinue.Update(a_gameTime);
            Instance.m_btnPlay.Update(a_gameTime);
            Instance.m_btnControls.Update(a_gameTime);
            Instance.m_btnExit.Update(a_gameTime);
            
            Instance.m_btnBack.Update(a_gameTime);
        }

        /// <summary>
        /// Draw intro game on screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void Draw(SpriteBatch a_spriteBatch)
        {
            if (!Instance.m_isVisbile)
            {
                return;
            }

            switch (Instance.m_ECurrentActiveScreen)
            {
                case EStartScreen.PlayScreen:
                    Instance.DrawPlayScreen(a_spriteBatch);
                    Instance.m_btnBack.ResetEvent();

                    break;
                case EStartScreen.ControlsScreen:
                    Instance.DrawControlsScreen(a_spriteBatch);
                    break;
                default:
                    break;
            }

            
        }

        /// <summary>
        /// Draw play screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        private void DrawPlayScreen(SpriteBatch a_spriteBatch)
        {
            switch (Instance.m_ECurrentSelectedBtn)
            {
                case EStartScreenOptions.Continue:
                    Instance.m_btnContinue.SetHighlightedState();
                    Instance.m_btnPlay.SetNormalState();
                    Instance.m_btnControls.SetNormalState();
                    Instance.m_btnExit.SetNormalState();
                    break;

                case EStartScreenOptions.Play:
                    Instance.m_btnContinue.SetNormalState();
                    Instance.m_btnPlay.SetHighlightedState();
                    Instance.m_btnControls.SetNormalState();
                    Instance.m_btnExit.SetNormalState();
                    break;

                case EStartScreenOptions.Controls:
                    Instance.m_btnContinue.SetNormalState();
                    Instance.m_btnPlay.SetNormalState();
                    Instance.m_btnControls.SetHighlightedState();
                    Instance.m_btnExit.SetNormalState();
                    break;

                case EStartScreenOptions.Exit:
                    Instance.m_btnContinue.SetNormalState();
                    Instance.m_btnPlay.SetNormalState();
                    Instance.m_btnControls.SetNormalState();
                    Instance.m_btnExit.SetHighlightedState();
                    break;

                default:

                    break;
            }

            Instance.m_objBg.Draw(a_spriteBatch);

            if(Instance.m_isContinueBtnVisbile)
            //if (SavingManager.OldSessionExists)
            {
                Instance.m_btnContinue.Draw(a_spriteBatch);
            }

            Instance.m_btnPlay.Draw(a_spriteBatch);
            Instance.m_btnControls.Draw(a_spriteBatch);
            Instance.m_btnExit.Draw(a_spriteBatch);
        }

        /// <summary>
        /// Draw controls screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        private void DrawControlsScreen(SpriteBatch a_spriteBatch)
        {
            Instance.m_btnBack.SetHighlightedState();
            Instance.m_objControlsBg.Draw(a_spriteBatch);
            Instance.m_btnBack.Draw(a_spriteBatch);;
        }
    }
}
