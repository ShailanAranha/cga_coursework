﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class InputManager
    {

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static InputManager s_instance = null;
        private static InputManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new InputManager();
                }

                return s_instance;
            }
        }

        /// </summary>
        /// Keyboard states used to determine key presses
        /// </summary>
        private KeyboardState m_kbCurrentKeyboardState;
        private KeyboardState m_kbPreviousKeyboardState;

        public static KeyboardState CurrentKeyboardState
        {
            get
            {
                return Instance.m_kbCurrentKeyboardState;
            }
        }

        public static KeyboardState PreviousKeyboardState
        {
            get
            {
                return Instance.m_kbPreviousKeyboardState;
            }
        }

        /// <summary>
        /// Gamepad states used to determine button presses
        /// </summary>
        private GamePadState m_gpCurrentGamePadState;
        private GamePadState m_gpPreviousGamePadState;

        public static GamePadState CurrentGamePadState
        {
            get
            {
                return Instance.m_gpCurrentGamePadState;
            }
        }

        public static GamePadState PreviousGamePadState
        {
            get
            {
                return Instance.m_gpPreviousGamePadState;
            }
        }

        /// <summary>
        /// Mouse States used to track mouse button press
        /// </summary>
        private MouseState m_mosCurrentMouseState;
        private MouseState m_mosPreviousMouseState;

        public static MouseState CurrentMouseState
        {
            get
            {
                return Instance.m_mosCurrentMouseState;
            }
        }

        public static MouseState PreviousMouseState
        {
            get
            {
                return Instance.m_mosPreviousMouseState;
            }
        }

        /// <summary>
        /// Event invoked when the player presses up on keyboard or dpad
        /// </summary>
        public static event Action OnPressedUp = null;

        /// <summary>
        /// Event invoked when the player presses down on keyboard or dpad
        /// </summary>
        public static event Action OnPressedDown = null;

        /// <summary>
        /// Event invoked when the player presses enter on keyboard or A on controller
        /// </summary>
        public static event Action OnPressedContinueBtn = null;

        /// <summary>
        /// Event invoked when the player presses space on keyboard or X on controller
        /// </summary>
        public static event Action OnPressedOpenBtn = null;

        /// <summary>
        /// Event invoked when the player presses a number on the keyboard
        /// </summary>
        public static event Action<char> OnEnterPinNum = null;

        /// <summary>
        /// Event invoked when the player presses C on keyboard or Y on controller
        /// </summary>
        public static event Action OnPressedTestingKey = null;

        /// <summary>
        /// For all the below booleans, True when user input is given and false when that input is released (TO AVOID MULTIPLE INPUTS ON A SINGLE PRESS)
        /// </summary>
        private bool IsUserInputGiven { get; set; } = false;
        private bool IsPressedUpEventFired { get; set; } = false;
        private bool IsPressedDownEventFired { get; set; } = false;
        private bool IsPressedContinueEventFired { get; set; } = false;
        private bool IsPressedOpenEventFired { get; set; } = false;
        private bool IsPressedTestingKeyFired { get; set; } = false;


        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private InputManager()
        {

        }

        /// <summary>
        /// Game loop to check all the user inputs
        /// </summary>
        /// <param name="gameTime"></param>
        public static void Update(GameTime gameTime)
        {

            // Save the previous state of the keyboard, game pad, and mouse so we can determine key / button presses
            Instance.m_gpPreviousGamePadState = Instance.m_gpCurrentGamePadState;
            Instance.m_kbPreviousKeyboardState = Instance.m_kbCurrentKeyboardState;
            Instance.m_mosPreviousMouseState = Instance.m_mosCurrentMouseState;

            // Read the current state and store it
            Instance.m_kbCurrentKeyboardState = Keyboard.GetState();
            Instance.m_gpCurrentGamePadState = GamePad.GetState(PlayerIndex.One);
            Instance.m_mosCurrentMouseState = Mouse.GetState();


            if (CurrentKeyboardState.IsKeyDown(Keys.Enter) || CurrentGamePadState.Buttons.A == ButtonState.Pressed)
            {
                if (!Instance.IsPressedContinueEventFired)
                {
                    OnPressedContinueBtn?.Invoke();
                    Instance.IsPressedContinueEventFired = true;
                }
            }

            if (CurrentKeyboardState.IsKeyDown(Keys.Space) || CurrentGamePadState.Buttons.X == ButtonState.Pressed)
            {
                if (!Instance.IsPressedOpenEventFired)
                {
                    OnPressedOpenBtn?.Invoke();
                    Instance.IsPressedOpenEventFired = true;
                }
            }

            if (CurrentKeyboardState.IsKeyUp(Keys.Enter) && CurrentGamePadState.Buttons.A == ButtonState.Released)
            {
                Instance.IsPressedContinueEventFired = false;
            }

            if (CurrentKeyboardState.IsKeyUp(Keys.Space) && CurrentGamePadState.Buttons.X == ButtonState.Released)
            {

                Instance.IsPressedOpenEventFired = false;
            }
           

            if (CurrentKeyboardState.IsKeyDown(Keys.NumPad0))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('0');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad1))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('1');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad2))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('2');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad3))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('3');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad4))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('4');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad5))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('5');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad6))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('6');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad7))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('7');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad8))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('8');
                    Instance.IsUserInputGiven = true;
                }
            }
            else if (CurrentKeyboardState.IsKeyDown(Keys.NumPad9))
            {
                if (!Instance.IsUserInputGiven)
                {
                    OnEnterPinNum?.Invoke('9');
                    Instance.IsUserInputGiven = true;
                }
            }
            else
            {
                Instance.IsUserInputGiven = false;
            }

            if (CurrentKeyboardState.IsKeyDown(Keys.Up) || CurrentGamePadState.DPad.Up == ButtonState.Pressed)
            {
                if (!Instance.IsPressedUpEventFired)
                { 
                    OnPressedUp?.Invoke();
                    Instance.IsPressedUpEventFired = true;
                }
            }

            if (CurrentKeyboardState.IsKeyDown(Keys.Down) || CurrentGamePadState.DPad.Down == ButtonState.Pressed)
            {
                if (!Instance.IsPressedDownEventFired)
                {
                    OnPressedDown?.Invoke();
                    Instance.IsPressedDownEventFired = true;
                }
            }

            if (CurrentKeyboardState.IsKeyUp(Keys.Up) && CurrentGamePadState.DPad.Up == ButtonState.Released)
            {
                Instance.IsPressedUpEventFired = false;
            }

            if (CurrentKeyboardState.IsKeyUp(Keys.Down) && CurrentGamePadState.DPad.Down == ButtonState.Released)
            {
                Instance.IsPressedDownEventFired = false;
            }

            if (CurrentKeyboardState.IsKeyDown(Keys.C) || CurrentGamePadState.Buttons.Y == ButtonState.Pressed)
            {
                if (!Instance.IsPressedTestingKeyFired)
                {
                    OnPressedTestingKey?.Invoke();
                    Instance.IsPressedTestingKeyFired = true;
                }
            }

            if (CurrentKeyboardState.IsKeyUp(Keys.C) && CurrentGamePadState.Buttons.Y == ButtonState.Released)
            {
                Instance.IsPressedTestingKeyFired = false;
            }

            //if (CurrentKeyboardState.IsKeyDown(Keys.G))
            //{
            //    FSMGameManager.ExitGameState(EGameState.MainGame);
            //    FSMGameManager.EnterGameState(EGameState.EndScreen);

            //}
            

            //if (CurrentKeyboardState.IsKeyDown(Keys.G))
            //{

            //    GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message2);
            //}

            //if (CurrentKeyboardState.IsKeyDown(Keys.G))
            //{

            //OverlayManager.DisplayNarrative("Page1");
            //    GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message2);
            //}

            //if (CurrentKeyboardState.IsKeyDown(Keys.H))
            //{
            //    GuidanceManager.HideGuidanceMessage();
            //}
        }
    }
}
