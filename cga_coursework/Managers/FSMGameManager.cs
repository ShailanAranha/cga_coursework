﻿
namespace cga_coursework
{
    class FSMGameManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static FSMGameManager s_instance = null;
        private static FSMGameManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new FSMGameManager();
                }


                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private FSMGameManager()
        {
        }

        /// <summary>
        /// Current game state
        /// </summary>
        private EGameState m_currentState = EGameState.IntroScreen;
        public static EGameState GetCurrentState
        {
            get
            {
                return Instance.m_currentState;
            }
        }

        /// <summary>
        /// FSM INIT where the game begins
        /// </summary>
        public static void Init()
        { 
           EnterGameState(EGameState.IntroScreen);
        }

        /// <summary>
        /// Enter game state using state type
        /// </summary>
        /// <param name="a_state"></param>
        public static void EnterGameState(EGameState a_state)
        {
            switch (a_state)
            {
                case EGameState.IntroScreen:
                    IntroScreenManager.ShowUI();
                    break;
                case EGameState.MainGame:
                    MainGameManager.ShowMainGame();
                    break;
                case EGameState.EndScreen:
                    EndScreenManager.ShowUI();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Exit game state using state type
        /// </summary>
        /// <param name="a_state"></param>
        public static void ExitGameState(EGameState a_state)
        {
            switch (a_state)
            {
                case EGameState.IntroScreen:
                    IntroScreenManager.HideUI();

                    break;
                case EGameState.MainGame:
                    MainGameManager.HideMainGame();
                    break;
                case EGameState.EndScreen:
                    EndScreenManager.HideUI();
                    break;
                default:
                    break;
            }
        }
    }

}
