﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace cga_coursework
{
    class OverlayManager
    {
        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static OverlayManager s_instance = null;
        private static OverlayManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new OverlayManager();
                }


                return s_instance;
            }
        }

        /// <summary>
        /// Default Contructor made private (protection level) to restrict any instance outside the class
        /// </summary>
        private OverlayManager()
        {
        }

        /// <summary>
        /// Current clue on the screen
        /// </summary>
        private Clue m_currentActiveClue = null;

        /// <summary>
        /// Current narrative on the screen
        /// </summary>
        private NarrativePage m_currentActiveNarrative = null;

        /// <summary>
        /// Current image on the screen
        /// </summary>
        private GameObject m_currentDisplayImage = null;

        /// <summary>
        /// Transperent background
        /// </summary>
        private GameObject m_imgTransparentBg = null;

        /// <summary>
        /// Current text on the screen
        /// </summary>
        private DisplayTextData m_currentDisplayText = null;

        /// <summary>
        /// Collection of pages for the current narrative on the screen
        /// </summary>
        private List<NarrativePage> m_lstCurrentNarrative = new List<NarrativePage>();

        /// <summary>
        /// Invoked when the final page of the narrative is hidden
        /// </summary>
        private Action CurrentNarrativeFinishedCallback = null;

        /// <summary>
        /// Initiaze
        /// </summary>
        /// <param name="a_transperantBg"></param>
        public static void Initiatize(Texture2D a_transperantBg)
        {
            Instance.m_imgTransparentBg = new GameObject("TrasparentBg", Vector2.Zero, new Vector2(50,50));
            Instance.m_imgTransparentBg.Initialize(a_transperantBg);
        }

        /// <summary>
        /// Load clue via clue id
        /// </summary>
        /// <param name="a_strClueId"></param>
        /// <param name="a_spriteBatch"></param>
        private static void LoadClueViaId(string a_strClueId, SpriteBatch a_spriteBatch)
        {
            IReadOnlyList<Clue> l_lstClue = GameDataManager.LstClueData;

            bool l_isClueIdFound = false;

            for (int i = 0; i < l_lstClue.Count; i++)
            {
                if (l_lstClue[i].ClueId.Equals(a_strClueId))
                {
                    LoadClue(l_lstClue[i], a_spriteBatch);
                    l_isClueIdFound = true;
                    break;
                }
            }

            if (!l_isClueIdFound)
            {
                Console.WriteLine("ERROR::OVERLAYMANAGER:: Clue ID not found: " + a_strClueId);
            }
        }

        /// <summary>
        /// Load clue contents
        /// </summary>
        /// <param name="a_Clue"></param>
        /// <param name="a_spriteBatch"></param>
        private static void LoadClue(Clue a_Clue, SpriteBatch a_spriteBatch)
        {
            // make a folder inside contents called clues and load textures from that


            // show spite and text (adjust the height of the image or to keep it simple give a provision directly inside XML to set height and width along with texture id as xml attribute maybe)
            // 1. Use gameobject class to image 
            // 2. Use DisplayTextData class to for text

            //GameObject l_obj = new GameObject(a_Clue.ClueId, Vector2.Zero, false);

            if (Instance.m_currentDisplayImage == null)
            {

                string[] l_arrDataPos = a_Clue.ImagePostion.Split(',');

                float l_fltXPosition = 0.0f;
                float l_fltYPosition = 0.0f;

                if (!float.TryParse(l_arrDataPos[0], out l_fltXPosition))
                {
                    ; // TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataPos[1], out l_fltYPosition))
                {
                    ;// TRY PARSE FAILED
                }

                Vector2 l_vec2DisplayPosition = new Vector2(l_fltXPosition, l_fltYPosition);

                string[] l_arrDataSclase = a_Clue.ImageDimensions.Split(',');

                float l_fltXScale = 0.0f;
                float l_fltYScale = 0.0f;

                if (!float.TryParse(l_arrDataSclase[0], out l_fltXScale))
                {
                    ;// TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataSclase[1], out l_fltYScale))
                {
                    ; // TRY PARSE FAILED
                }

                Vector2 l_vec2DisplayScale = new Vector2(l_fltXScale, l_fltYScale);


                Instance.m_currentDisplayImage = new GameObject(a_Clue.ClueId, l_vec2DisplayPosition, l_vec2DisplayScale);

                IReadOnlyList<TextureData> l_lsttextureData = GameDataManager.LstTextureData;
                bool l_isTextureFound = false;
                for (int i = 0; i < l_lsttextureData.Count; i++)
                {
                    if (l_lsttextureData[i].Id.Equals(a_Clue.TextureId))
                    {
                        Instance.m_currentDisplayImage.Initialize(l_lsttextureData[i].TexImg);
                        l_isTextureFound = true;
                        break;      // Break the loop
                    }
                }

                if (!l_isTextureFound)
                {
                    Console.WriteLine("ERROR::OVERLAYMANAGER:: Texture not found: " + a_Clue.TextureId);
                }

            }

            Instance.m_currentDisplayImage.Draw(a_spriteBatch);

            //DisplayTextData l_DisplayText = new DisplayTextData("", a_Clue.DisplayText, new Vector2(500, 500), Color.White);
            //HudManager.RegisterTextOnHud(l_DisplayText);

            if (Instance.m_currentDisplayText == null)
            {
                string[] l_arrDataPos = a_Clue.TextPostion.Split(',');

                float l_fltXPosition = 0.0f;
                float l_fltYPosition = 0.0f;

                if (!float.TryParse(l_arrDataPos[0], out l_fltXPosition))
                {
                    ; // TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataPos[1], out l_fltYPosition))
                {
                    ;// TRY PARSE FAILED
                }

                Vector2 l_vec2TextPosition = new Vector2(l_fltXPosition, l_fltYPosition);

                Instance.m_currentDisplayText = new DisplayTextData("", a_Clue.DisplayText, l_vec2TextPosition, Color.LightSeaGreen);
                HudManager.RegisterTextOnHud(Instance.m_currentDisplayText);


                AudioManager.StopSFX(GameAudio.OPEN_CLUE);
                AudioManager.PlaySFX(GameAudio.OPEN_CLUE);
            }
        }

        /// <summary>
        /// Draw clue on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void DrawClue(SpriteBatch a_spriteBatch)
        {
            /*
            // Caching Data
            GameData l_gamedata= GameDataManager.GameData;
            PinClue l_pinClue = l_gamedata.PinClue;

            if (l_pinClue.FirstClue.IsDisplayClue)
            {
                DisplayClueViaId(l_pinClue.FirstClue.ClueId, a_spriteBatch);
            }
            */
            IReadOnlyList<Clue> l_lstClue = GameDataManager.LstClueData;

            for (int i = 0; i < l_lstClue.Count; i++)
            {
                if (l_lstClue[i].IsDisplayClue)
                {
                    Instance.m_imgTransparentBg.Draw(a_spriteBatch);
                    LoadClueViaId(l_lstClue[i].ClueId, a_spriteBatch);
                }
            }
        }

        /// <summary>
        /// Display guidance message to the player to indicate its a clue
        /// </summary>
        /// <param name="a_strInteractiveObjectId"></param>
        public static void DisplayClue(string a_strInteractiveObjectId)
        {

            if (a_strInteractiveObjectId.Equals(InteractableObjects.Painting1_ID))
            {
                //GameDataManager.GameData.PinClue.FirstClue.IsDisplayClue = true;
                Instance.m_currentActiveClue = GameDataManager.GameData.PinClue.FirstClue;
                Instance.m_currentActiveClue.IsDisplayClue = true;
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);
                PlayerManager.SetPlayerMovement(false);
            }

            if (a_strInteractiveObjectId.Equals(InteractableObjects.Painting2_ID))
            {
                //GameDataManager.GameData.PinClue.FirstClue.IsDisplayClue = true;
                Instance.m_currentActiveClue = GameDataManager.GameData.PinClue.SecondClue;
                Instance.m_currentActiveClue.IsDisplayClue = true;
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);
                PlayerManager.SetPlayerMovement(false);
            }

            if (a_strInteractiveObjectId.Equals(InteractableObjects.Painting3_ID))
            {
                //GameDataManager.GameData.PinClue.FirstClue.IsDisplayClue = true;
                Instance.m_currentActiveClue = GameDataManager.GameData.PinClue.ThirdClue;
                Instance.m_currentActiveClue.IsDisplayClue = true;
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);
                PlayerManager.SetPlayerMovement(false);
            }

            if (a_strInteractiveObjectId.Equals(InteractableObjects.Table_ID))
            {
                //GameDataManager.GameData.PinClue.FirstClue.IsDisplayClue = true;
                Instance.m_currentActiveClue = GameDataManager.GameData.KeyClue.Clue;
                Instance.m_currentActiveClue.IsDisplayClue = true;
                GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);
                PlayerManager.SetPlayerMovement(false);
            }


            InputManager.OnPressedContinueBtn -= HideCurrentInstruction;
            InputManager.OnPressedContinueBtn += HideCurrentInstruction;
        }

        #region NARRATIVE

        /// <summary>
        /// Load narrative via narrative id
        /// </summary>
        /// <param name="a_strNarrativeId"></param>
        /// <param name="a_spriteBatch"></param>
        private static void LoadNarrativeViaId(string a_strNarrativeId, SpriteBatch a_spriteBatch)
        {
            LoadNarrative(Instance.m_currentActiveNarrative, a_spriteBatch);
        }

        /// <summary>
        /// Load narrative contents
        /// </summary>
        /// <param name="a_narrative"></param>
        /// <param name="a_spriteBatch"></param>
        private static void LoadNarrative(NarrativePage a_narrative, SpriteBatch a_spriteBatch)
        {
            if (Instance.m_currentDisplayImage == null)
            {

                string[] l_arrDataPos = a_narrative.BgPostion.Split(',');

                float l_fltXPosition = 0.0f;
                float l_fltYPosition = 0.0f;

                if (!float.TryParse(l_arrDataPos[0], out l_fltXPosition))
                {
                    ; // TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataPos[1], out l_fltYPosition))
                {
                    ;// TRY PARSE FAILED
                }

                Vector2 l_vec2DisplayPosition = new Vector2(l_fltXPosition, l_fltYPosition);

                string[] l_arrDataSclase = a_narrative.BgDimensions.Split(',');

                float l_fltXScale = 0.0f;
                float l_fltYScale = 0.0f;

                if (!float.TryParse(l_arrDataSclase[0], out l_fltXScale))
                {
                    ;// TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataSclase[1], out l_fltYScale))
                {
                    ; // TRY PARSE FAILED
                }

                Vector2 l_vec2DisplayScale = new Vector2(l_fltXScale, l_fltYScale);

                Instance.m_currentDisplayImage = new GameObject(a_narrative.Id, l_vec2DisplayPosition, l_vec2DisplayScale);
                Instance.m_currentDisplayImage.Initialize(GameDataManager.NarrativeBg);
            }

            Instance.m_currentDisplayImage.Draw(a_spriteBatch);

            if (Instance.m_currentDisplayText == null)
            {
                string[] l_arrDataPos = a_narrative.TextPostion.Split(',');

                float l_fltXPosition = 0.0f;
                float l_fltYPosition = 0.0f;

                if (!float.TryParse(l_arrDataPos[0], out l_fltXPosition))
                {
                    ; // TRY PARSE FAILED
                }

                if (!float.TryParse(l_arrDataPos[1], out l_fltYPosition))
                {
                    ;// TRY PARSE FAILED
                }

                Vector2 l_vec2TextPosition = new Vector2(l_fltXPosition, l_fltYPosition);

                Instance.m_currentDisplayText = new DisplayTextData("", a_narrative.DisplayText, l_vec2TextPosition, Color.White);
                HudManager.RegisterTextOnHud(Instance.m_currentDisplayText);

                AudioManager.StopSFX(GameAudio.PAGE_TURN);
                AudioManager.PlaySFX(GameAudio.PAGE_TURN);
            }
        }

        /// <summary>
        /// Draw narrative on the screen
        /// </summary>
        /// <param name="a_spriteBatch"></param>
        public static void DrawNarrative(SpriteBatch a_spriteBatch)
        {
            /*
            IReadOnlyList<Narrative> l_lstNarrative = GameDataManager.LstNarrativeData;

            for (int i = 0; i < l_lstNarrative.Count; i++)
            {
                if (l_lstNarrative[i].IsDisplay)
                {
                    LoadNarrativeViaId(l_lstNarrative[i].Id, a_spriteBatch);
                }
            }
             */

            IReadOnlyList<NarrativePage> l_lstNarrative = Instance.m_lstCurrentNarrative;
            
            for (int i = 0; i < l_lstNarrative.Count; i++)
            {
                if (l_lstNarrative[i].IsDisplay)
                {
                    LoadNarrativeViaId(l_lstNarrative[i].Id, a_spriteBatch);
                }
            }

        }


        /// <summary>
        /// Display guidance message to the player to indicate its a narrative
        /// </summary>
        /// <param name="a_type"></param>
        /// <param name="a_callback"></param>
        public static void DisplayNarrative(ENarrativeType a_type, Action a_callback = null)
        {

            Instance.CurrentNarrativeFinishedCallback = a_callback;
            IReadOnlyList<NarrativePage> l_lstNarrative = GameDataManager.LstNarrativeData;
            
            Instance.m_lstCurrentNarrative = new List<NarrativePage>();
            for (int i = 0; i < l_lstNarrative.Count; i++)
            {
                if (l_lstNarrative[i].Type.Equals(a_type))
                {
                    Instance.m_lstCurrentNarrative.Add(l_lstNarrative[i]);
                }
            }

            Instance.m_currentActiveNarrative = Instance.m_lstCurrentNarrative[0];
            Instance.m_currentActiveNarrative.IsDisplay = true;
            Instance.m_currentActiveNarrative.IsAlreadyDisplayed = true;

            GuidanceManager.ShowGuidanceMessage(EGuidanceMessage.Message1);

            PlayerManager.SetPlayerMovement(false);

            InputManager.OnPressedContinueBtn -= ShowNextPage;
            InputManager.OnPressedContinueBtn += ShowNextPage;
        }

        /// <summary>
        /// Show next page of the narrative
        /// </summary>
        private static void ShowNextPage()
        {
            bool l_isNextPageExits = false;
            for (int i = 0; i < Instance.m_lstCurrentNarrative.Count; i++)
            {
                if (!Instance.m_lstCurrentNarrative[i].IsAlreadyDisplayed)
                {
                    Instance.m_currentActiveNarrative.IsDisplay = false;

                    HudManager.UnregisterTextOnHud(Instance.m_currentDisplayText);
                    Instance.m_currentDisplayText = null;
                    Instance.m_currentDisplayImage = null;
                    Instance.m_currentActiveNarrative = null;
                    Instance.m_currentActiveNarrative = Instance.m_lstCurrentNarrative[i];
                    Instance.m_currentActiveNarrative.IsDisplay = true;
                    Instance.m_currentActiveNarrative.IsAlreadyDisplayed = true;

                   // m_temp3.DisplayText += Instance.m_lstCurrentNarrative[i].Id;

                    //Instance.m_currentDisplayText.DisplayText = Instance.m_currentActiveNarrative.DisplayText;

                    l_isNextPageExits = true;
                    break;
                }
            }

            if (!l_isNextPageExits)
            {
                HideCurrentInstruction();

                InputManager.OnPressedContinueBtn -= ShowNextPage;
                Instance.CurrentNarrativeFinishedCallback?.Invoke();
                Instance.CurrentNarrativeFinishedCallback = null;
            }

        }

        #endregion

        /// <summary>
        /// Hide currently active clue or narrative
        /// </summary>
        public static void HideCurrentInstruction()
        {
            if (Instance.m_currentActiveClue != null)
            {
                Instance.m_currentActiveClue.IsDisplayClue = false;
            }

            if (Instance.m_currentDisplayText != null)
            {
                HudManager.UnregisterTextOnHud(Instance.m_currentDisplayText);
            }

            if (Instance.m_currentActiveNarrative != null)
            { 
                Instance.m_currentActiveNarrative.IsDisplay = false;
            }

            Instance.m_currentDisplayImage = null;
            Instance.m_currentDisplayText = null;
            Instance.m_currentActiveNarrative = null;
            InputManager.OnPressedContinueBtn -= HideCurrentInstruction;

            GuidanceManager.HideGuidanceMessage();
            PlayerManager.SetPlayerMovement(true);
        }

    }


}
